/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.serpro.expresso.security.exception;

/**
 *
 * @author Humberto
 */
public class ExpressoCertificateException extends Exception {

    public ExpressoCertificateException() {
    }

    public ExpressoCertificateException(String message) {
        super(message);
    }

    public ExpressoCertificateException(String message, Throwable cause) {
        super(message, cause);
    }
}
