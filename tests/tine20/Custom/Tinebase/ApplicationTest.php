<?php

/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Application
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 SERPRO
 * @author      Jeferson Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test class for Tinebase_Group
 */
class Custom_Tinebase_ApplicationTest extends Tinebase_ApplicationTest
{

    /**
     * Test length name for table name and column name (Oracle Database limitation)
     *
     * @see 0007452: use json encoded array for saving of policy settings
     */
    public function testSetupXML()
    {
        $applications = Tinebase_Application::getInstance()->getApplications();

        foreach ($applications->name as $applicationName) {
            // skip ActiveSync
            // @todo remove that when #7452 is resolved
            if ($applicationName === 'ActiveSync') {
                continue;
            }

            $xml = Setup_Controller::getInstance()->getSetupXml($applicationName);
            if (isset($xml->tables)) {
                foreach ($xml->tables[0] as $tableXML) {
                    $table = Setup_Backend_Schema_Table_Factory::factory('Xml', $tableXML);
                    $currentTable = $table->name;
                    if (Tinebase_Core::getDb() instanceof Tinebase_Backend_Sql_Adapter_Pdo_Oci) {
                        $this->assertLessThan(29, strlen($currentTable), $applicationName . " -> " . $table->name . "  (" . strlen($currentTable) . ")");
                    } else {
                        $this->assertLessThan(30, strlen($currentTable), $applicationName . " -> " . $table->name . "  (" . strlen($currentTable) . ")");
                    }
                    foreach ($table->fields as $field) {
                        $this->assertLessThan(31, strlen($field->name), $applicationName . " -> " . $table->name . "  (" . strlen($field->name) . ")");
                    }
                }
            }
        }
    }

}
