<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Container
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * Custom tests for ContainerTest
 *
 */
class Custom_Tinebase_ContainerTest extends Tinebase_ContainerTest
{

    /**
     * custom test for testSetGrants
     *
     * problems when there are no group
     * so we set account_id (group_id) to 0 for grants and the test can be run
     */
    public function testSetGrants()
    {
        $newGrants = new Tinebase_Record_RecordSet('Tinebase_Model_Grants');
        $newGrants->addRecord(
            new Tinebase_Model_Grants(array(
                    'account_id'     => Tinebase_Core::getUser()->getId(),
                    'account_type'   => 'user',
                    Tinebase_Model_Grants::GRANT_READ      => true,
                    Tinebase_Model_Grants::GRANT_ADD       => false,
                    Tinebase_Model_Grants::GRANT_EDIT      => true,
                    Tinebase_Model_Grants::GRANT_DELETE    => true,
                    Tinebase_Model_Grants::GRANT_ADMIN     => true
             ))
        );

        // add grants for group
        $newGrants->addRecord(
            new Tinebase_Model_Grants(array(
                    'account_id'     => 0,
                    'account_type'   => 'group',
                    Tinebase_Model_Grants::GRANT_READ      => true,
                    Tinebase_Model_Grants::GRANT_ADD       => false,
                    Tinebase_Model_Grants::GRANT_EDIT      => false,
                    Tinebase_Model_Grants::GRANT_DELETE    => false,
                    Tinebase_Model_Grants::GRANT_ADMIN     => false
             ))
        );

        $grants = $this->_instance->setGrants($this->objects['initialContainer'], $newGrants);
        $this->assertEquals('Tinebase_Record_RecordSet', get_class($grants), 'wrong type');
        $this->assertEquals(2, count($grants));

        $grants = $grants->toArray();
        foreach ($grants as $grant) {
            if ($grant['account_id'] === Tinebase_Core::getUser()->getId()) {
                $this->assertTrue($grant["readGrant"], print_r($grant, TRUE));
                $this->assertFalse($grant["addGrant"], print_r($grant, TRUE));
                $this->assertTrue($grant["editGrant"], print_r($grant, TRUE));
                $this->assertTrue($grant["deleteGrant"], print_r($grant, TRUE));
                $this->assertTrue($grant["adminGrant"], print_r($grant, TRUE));
                $this->assertEquals(Tinebase_Acl_Rights::ACCOUNT_TYPE_USER, $grant['account_type']);
            } else {
                $this->assertTrue($grant["readGrant"], print_r($grant, TRUE));
                $this->assertEquals(Tinebase_Acl_Rights::ACCOUNT_TYPE_GROUP, $grant['account_type']);
            }
        }
    }

    /**
     * custom test for testOverwriteGrantsWithAddGrants
     *
     * we don't add sync grant by default, so grants count must be 6 instead of 7
     *
     */
    public function testOverwriteGrantsWithAddGrants()
    {
        $result = $this->_instance->addGrants($this->objects['initialContainer'], 'user', Tinebase_Core::getUser()->getId(), array(Tinebase_Model_Grants::GRANT_ADMIN));
        $this->assertTrue($result);

        // check num of db rows
        $stmt = Tinebase_Core::getDb()->query('select * from '.Tinebase_Core::getDb()->quoteIdentifier(SQL_TABLE_PREFIX .'container_acl') . ' where ' . Tinebase_Core::getDb()->quoteInto(Tinebase_Core::getDb()->quoteIdentifier('container_id') . ' = ?', $this->objects['initialContainer']->getId()));
        $rows = $stmt->fetchAll();

        $this->assertEquals(6, count($rows));
    }

}
