<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom Test class for Tinebase_CustomField
 */
class Custom_Tinebase_CustomFieldTest extends Tinebase_CustomFieldTest
{
    /**
     * Customized Sets up the fixture.
     * Expresso does not require reset of numeric formatation,
     *   as it do not has Sales module.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        $this->_transactionId = Tinebase_TransactionManager::getInstance()->startTransaction(Tinebase_Core::getDb());
        $this->_instance = Tinebase_CustomField::getInstance();
    }
}
