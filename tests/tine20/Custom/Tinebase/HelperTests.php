<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * Test class for Tinebase_Helper
 */
class Custom_Tinebase_HelperTests extends Tinebase_HelperTests
{

    public function testArrayToCacheId()
    {
        $hash = Tinebase_Helper::arrayToCacheId(array('foo' => 'bar'));
        if(Tinebase_Config_Manager::isMultidomain()) {
            if(Tinebase_Config::getDomain() == 'serpro.gov.br') {
                $this->assertEquals('10490aeb1a524293acae3ece4c203e26', $hash);
            } else {
                $this->markTestSkipped("Invalid domain to make this test");
            }
        } else {
            $this->assertEquals('37b51d194a7513e45b56f6524f2d51f2', $hash);
        }

        $hash = Tinebase_Helper::arrayToCacheId(array('foo' => 'bar'), true);
        if(Tinebase_Config_Manager::isMultidomain()) {
            if(Tinebase_Config::getDomain() == 'serpro.gov.br') {
                $this->assertEquals('cf48fb3daeb7bf7f08b474a2c268167e', $hash);
            } else {
                $this->markTestSkipped("Invalid domain to make this test");
            }
        } else {
            $this->assertEquals('3858f62230ac3c915f300c664312c63f', $hash);
        }
    }

    public function testArrayHash()
    {
        $hash = Tinebase_Helper::arrayHash(array('foo' => 'bar'));
        if(Tinebase_Config_Manager::isMultidomain()) {
            if(Tinebase_Config::getDomain() == 'serpro.gov.br') {
                $this->assertEquals('10490aeb1a524293acae3ece4c203e26', $hash);
            } else {
                $this->markTestSkipped("Invalid domain to make this test");
            }
        } else {
            $this->assertEquals('37b51d194a7513e45b56f6524f2d51f2', $hash);
        }

        $hash = Tinebase_Helper::arrayHash(array('foo' => 'bar'), true);
        if(Tinebase_Config_Manager::isMultidomain()) {
            if(Tinebase_Config::getDomain() == 'serpro.gov.br') {
                $this->assertEquals('cf48fb3daeb7bf7f08b474a2c268167e', $hash);
            } else {
                $this->markTestSkipped("Invalid domain to make this test");
            }
        } else {
            $this->assertEquals('3858f62230ac3c915f300c664312c63f', $hash);
        }
    }
}