<?php

/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tests
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Tinebase TestSuite
 *
 * @package     Tests
 */
class TestSuite extends PHPUnit_Framework_TestSuite
{
    /**
     * Adds the tests from the given class to the suite.
     * If plugin class exists, it is used.
     *
     * @param  mixed $testClass
     */
    public function addTestSuite($testClass)
    {
         $testConfig = Zend_Registry::get('testConfig');

        if (isset($testConfig) && isset($testConfig->customtests)
           && isset($testConfig->customtests->enabled) && $testConfig->customtests->enabled === true) {
            $customClassName = 'Custom_' . $testClass;

            if (@class_exists($customClassName)) {
                parent::addTestSuite($customClassName);
            } else {
                parent::addTestSuite($testClass);
            }
        } else {
            parent::addTestSuite($testClass);
        }
    }
}
