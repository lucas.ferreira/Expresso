<?php
/**
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * AppLauncher Controller (composite)
 * 
 * The AppLauncher 2.0 Controller manages access (acl) to the different backends and supports
 * a common interface to the servers/views
 * 
 * @package AppLauncher
 * @subpackage  Controller
 */
class AppLauncher_Controller extends Tinebase_Controller_Abstract
{
    /**
     * holds the default Model of this application
     * @var string
     */
    protected static $_defaultModel = 'AppLauncher_Model_AppLauncherRecord';
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_applicationName = 'AppLauncher';
    }
    
    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone() 
    {
    }
    
    /**
     * holds self
     * @var AppLauncher_Controller
     */
    private static $_instance = NULL;
    
    /**
     * singleton
     *
     * @return AppLauncher_Controller
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new AppLauncher_Controller();
        }
        return self::$_instance;
    }

    /**
     * Gets App settings
     *
     * @return array
     */
    public function getSettings()
    {
        $config = AppLauncher_Config::getInstance();

        $domain = $config->get(AppLauncher_Config::DOMAIN);
        $externalApplications = $config->get(AppLauncher_Config::EXTERNAL_APPLICATIONS);
        
        return array(
            AppLauncher_Config::DOMAIN                => $domain,
            AppLauncher_Config::EXTERNAL_APPLICATIONS => $externalApplications
        );
    }

    /**
     * Save Settings 
     *
     * @param array $settings
     * @return bool
     */
    public function saveSettings($settings)
    {
        try {
            foreach($settings as $key => $value) {
                AppLauncher_Config::getInstance()->set($key, $value);
            }
            return true;
        } catch(Tinebase_Exception $ex) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Cannot save settings: ".$ex->getMessage());
            return false;
        }
    }
}
