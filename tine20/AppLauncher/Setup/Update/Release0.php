<?php
/**
 * Tine 2.0
 *
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2013 SERPRO (Serviço Federal de Processamento de Dados)
 * @author      Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>
 */

class AppLauncher_Setup_Update_Release0 extends Setup_Update_Abstract
{
    /**
     * drops the example_application_record table and updates the version of the application
     * 
     */
    public function update_1()
    {
        //example_application_record
        $this->dropTable('example_application_record', 'AppLauncher');
        $this->setApplicationVersion('AppLauncher', '1.0');
    }
}
