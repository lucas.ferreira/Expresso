/*
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.AppLauncher', 'Tine.AppLauncher.Model');

Tine.AppLauncher.Model.Settings = Tine.Tinebase.data.Record.create([
        {name: 'domain'},
        {name: 'adminlist'}
    ], {
    appName: 'AppLauncher',
    modelName: 'Settings',
    idProperty: 'id',
    titleProperty: 'title',
    recordName: 'Settings',
    recordsName: 'Settingss',
    containerName: 'Settings',
    containersName: 'Settings',
    getTitle: function() {
        return this.recordName;
    }
});

Tine.AppLauncher.settingsBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'AppLauncher',
    modelName: 'Settings',
    recordClass: Tine.AppLauncher.Model.Settings
});
