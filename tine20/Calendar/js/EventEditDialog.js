/*
 * Tine 2.0
 * 
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.ns('Tine.Calendar');

/**
 * @namespace Tine.Calendar
 * @class Tine.Calendar.EventEditDialog
 * @extends Tine.widgets.dialog.EditDialog
 * Calendar Edit Dialog <br>
 * 
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */
Tine.Calendar.EventEditDialog = Ext.extend(Tine.widgets.dialog.EditDialog, {
    /**
     * @cfg {Number} containerId initial container id
     */
    containerId: -1,
    

    labelAlign: 'side',
    windowNamePrefix: 'EventEditWindow_',
    appName: 'Calendar',
    recordClass: Tine.Calendar.Model.Event,
    recordProxy: Tine.Calendar.backend,
    showContainerSelector: false,
    trackResetOnLoad: true,
    mode: 'local',
    delegationAutoStart: false,
    isDelegation: false,

    // note: we need up use new action updater here or generally in the widget!
    evalGrants: false,
    
    onResize: function() {
        Tine.Calendar.EventEditDialog.superclass.onResize.apply(this, arguments);
        this.setTabHeight.defer(100, this);
    },

    onPrint: function (button, event){
      if(this.window.name == 'EventEditWindow_0')
       {
          Ext.Msg.show({
               msg        : Tine.Tinebase.appMgr.get('Calendar').i18n._('This event must be saved before it is printed.'),
               width      : 300,
               buttons    : Ext.MessageBox.OK,
               icon       : Ext.MessageBox.ERROR
         });
       }
      else
       {
        var renderer = new this.printRenderer();
        renderer.print(this);
       }
    },

    onSaveAndClose: function(button, event){
        this.handleNotification(button, event);
    },
     /**
     * generic apply changes handler
     */
    onApplyChanges: function(button, event, closeWindow) {
        // we need to sync record before validating to let (sub) panels have 
        // current data of other panels
        this.onRecordUpdate();
        if(this.isValid()) {
            this.loadMask.show();
            if (this.mode !== 'local') {
                this.fireEvent('save');
                this.recordProxy.saveRecord(this.record, {
                    scope: this,
                    success: function(record) {
                        // override record with returned data
                        this.record = record;
                        
                        if (! (closeWindow && typeof this.window.cascade == 'function')) {
                            // update form with this new data
                            // NOTE: We update the form also when window should be closed,
                            //       cause sometimes security restrictions might prevent
                            //       closing of native windows
                            this.onRecordLoad();
                        }
                        this.fireEvent('update', Ext.util.JSON.encode(this.record.data), this.mode);
                        
                        // free 0 namespace if record got created
                        this.window.rename(this.windowNamePrefix + this.record.id);
                        
                        if (closeWindow) {
                            this.purgeListeners();
                            this.window.close();
                        }
                    },
                    failure: this.onRequestFailed,
                    timeout: 300000 // 5 minutes
                }, {
                    duplicateCheck: this.doDuplicateCheck
                });
            } else {
                this.onRecordLoad();
                this.fireEvent('update', Ext.util.JSON.encode(this.record.data), this.mode);
                
                // free 0 namespace if record got created
                this.window.rename(this.windowNamePrefix + this.record.id);
                        
                if (closeWindow) {
                    this.purgeListeners();
                    this.window.close();
                }
            }
        } else {
            var msgerr = this.validationErrorMessage ? this.validationErrorMessage : this.getValidationErrorMessage();
            Ext.MessageBox.alert(_('Errors'), msgerr);
        }
    },   
/**
     * init attachment grid 
     */
    initAttachmentGrid: function() {
        if (! this.attachmentGrid) {
        
            this.attachmentGrid = new Tine.widgets.grid.FileUploadGrid({
                fieldLabel: this.app.i18n._('Attachments'),
                hideLabel: true,
                filesProperty: 'attachments',
                anchor: '100% 95%'
            });
        }
    },
 
    /**
     * returns dialog
     * 
     * NOTE: when this method gets called, all initalisation is done.
     * @return {Object} components this.itmes definition
     */
    getFormItems: function() {
        this.initAttachmentGrid();        
        return {
            xtype: 'tabpanel',
            border: false,
            plugins: [{
                ptype : 'ux.tabpanelkeyplugin'
            }],
            plain:true,
            activeTab: 0,
            border: false,
            items:[{
                title: this.app.i18n.n_('Event', 'Events', 1),
                border: false,
                frame: true,
                layout: 'border',
                items: [{
                    region: 'center',
                    layout: 'hfit',
                    border: false,
                    items: [{
                        layout: 'hbox',
                        items: [{
                            margins: '5',
                            width: 100,
                            xtype: 'label',
                            text: this.app.i18n._('Summary')
                        }, {
                            flex: 1,
                            xtype:'textfield',
                            name: 'summary',
                            listeners: {render: function(field){field.focus(false, 250);}},
                            allowBlank: false,
                            requiredGrant: 'editGrant',
                            maxLength: 255
                        }]
                    }, {
                        layout: 'hbox',
                        items: [{
                            margins: '5',
                            width: 100,
                            xtype: 'label',
                            text: this.app.i18n._('View')
                        }, Ext.apply(this.perspectiveCombo, {
                            flex: 1
                        })]
                    }, {
                        layout: 'hbox',
                        height: 135,
                        layoutConfig: {
                            align : 'stretch',
                            pack  : 'start'
                        },
                        items: [{
                            flex: 1,
                            xtype: 'fieldset',
                            layout: 'hfit',
                            margins: '0 5 0 0',
                            title: this.app.i18n._('Details'),
                            items: [{
                                xtype: 'columnform',
                                labelAlign: 'side',
                                labelWidth: 100,
                                formDefaults: {
                                    xtype:'textfield',
                                    anchor: '100%',
                                    labelSeparator: '',
                                    columnWidth: .7
                                },
                                items: [[{
                                    columnWidth: 1,
                                    fieldLabel: this.app.i18n._('Location'),
                                    name: 'location',
                                    requiredGrant: 'editGrant',
                                    maxLength: 255
                                }], [{
                                    xtype: 'datetimefield',
                                    fieldLabel: this.app.i18n._('Start Time'),
                                    listeners: {scope: this, change: this.onDtStartChange},
                                    name: 'dtstart',
                                    requiredGrant: 'editGrant'
                                }, {
                                    columnWidth: .19,
                                    xtype: 'checkbox',
                                    hideLabel: true,
                                    boxLabel: this.app.i18n._('whole day'),
                                    listeners: {scope: this, check: this.onAllDayChange},
                                    name: 'is_all_day_event',
                                    requiredGrant: 'editGrant'
                                }], [{
                                    xtype: 'datetimefield',
                                    fieldLabel: this.app.i18n._('End Time'),
                                    listeners: {scope: this, change: this.onDtEndChange},
                                    name: 'dtend',
                                    requiredGrant: 'editGrant'
                                }, {
                                    columnWidth: .3,
                                    xtype: 'combo',
                                    hideLabel: true,
                                    readOnly: true,
                                    hideTrigger: true,
                                    disabled: true,
                                    name: 'originator_tz',
                                    requiredGrant: 'editGrant'
                                }], [ this.containerSelectCombo = new Tine.widgets.container.selectionComboBox({
                                    columnWidth: 1,
                                    id: this.app.appName + 'EditDialogContainerSelector' + Ext.id(),
                                    fieldLabel: _('Saved in'),
                                    ref: '../../../../../../../../containerSelect',
                                    //width: 300,
                                    //listWidth: 300,
                                    name: this.recordClass.getMeta('containerProperty'),
                                    recordClass: this.recordClass,
                                    containerName: this.app.i18n.n_hidden(this.recordClass.getMeta('containerName'), this.recordClass.getMeta('containersName'), 1),
                                    containersName: this.app.i18n._hidden(this.recordClass.getMeta('containersName')),
                                    appName: this.app.appName,
                                    requiredGrant: 'readGrant',
                                    disabled: true
                                }), Ext.apply(this.perspectiveCombo.getAttendeeContainerField(), {
                                    columnWidth: 1
                                })], [Tine.widgets.form.RecordPickerManager.get('Addressbook', 'Contact', {
                                    columnWidth: .995,
                                    fieldLabel: this.app.i18n._('Organizer'),
                                    flex: 1,
                                    name: 'organizer',
                                    userOnly: true,
                                    getValue: function() {
                                        var id = Tine.Addressbook.SearchCombo.prototype.getValue.apply(this, arguments),
                                            record = this.store.getById(id);
                                        return record ? record.data : id;
                                    }
                                })]]
                            }]
                        }, {
                            width: 130,
                            xtype: 'fieldset',
                            title: this.app.i18n._('Status'),
                            items: [{
                                xtype: 'checkbox',
                                hideLabel: true,
                                boxLabel: this.app.i18n._('non-blocking'),
                                name: 'transp',
                                requiredGrant: 'editGrant',
                                getValue: function() {
                                    var bool = Ext.form.Checkbox.prototype.getValue.call(this);
                                    return bool ? 'TRANSPARENT' : 'OPAQUE';
                                },
                                setValue: function(value) {
                                    var bool = (value == 'TRANSPARENT' || value === true);
                                    return Ext.form.Checkbox.prototype.setValue.call(this, bool);
                                }
                            }, Ext.apply(this.perspectiveCombo.getAttendeeTranspField(), {
                                hideLabel: true
                            }), {
                                xtype: 'checkbox',
                                hideLabel: true,
                                boxLabel: this.app.i18n._('Tentative'),
                                name: 'status',
                                requiredGrant: 'editGrant',
                                getValue: function() {
                                    var bool = Ext.form.Checkbox.prototype.getValue.call(this);
                                    return bool ? 'TENTATIVE' : 'CONFIRMED';
                                },
                                setValue: function(value) {
                                    var bool = (value == 'TENTATIVE' || value === true);
                                    return Ext.form.Checkbox.prototype.setValue.call(this, bool);
                                }
                            }, {
                                xtype: 'checkbox',
                                hideLabel: true,
                                boxLabel: this.app.i18n._('Private'),
                                name: 'class',
                                requiredGrant: 'editGrant',
                                getValue: function() {
                                    var bool = Ext.form.Checkbox.prototype.getValue.call(this);
                                    return bool ? 'PRIVATE' : 'PUBLIC';
                                },
                                setValue: function(value) {
                                    var bool = (value == 'PRIVATE' || value === true);
                                    return Ext.form.Checkbox.prototype.setValue.call(this, bool);
                                }
                            }, Ext.apply(this.perspectiveCombo.getAttendeeStatusField(), {
                                width: 115,
                                hideLabel: true
                            })]
                        }]
                    }, {
                        xtype: 'tabpanel',
                        deferredRender: false,
                        activeTab: 0,
                        border: false,
                        height: 235,
                        form: true,
                        items: [
                            this.attendeeGridPanel,
                            this.rrulePanel,
                            this.alarmPanel,
                            this.attendeeFreebusyGridPanel
                        ]
                    }]
                }, {
                    // activities and tags
                    region: 'east',
                    layout: 'accordion',
                    animate: true,
                    width: 200,
                    split: true,
                    collapsible: true,
                    collapseMode: 'mini',
                    header: false,
                    margins: '0 5 0 5',
                    border: true,
                    items: [
                        new Ext.Panel({
                            // @todo generalise!
                            title: this.app.i18n._('Description'),
                            iconCls: 'descriptionIcon',
                            layout: 'form',
                            labelAlign: 'top',
                            border: false,
                            items: [{
                                style: 'margin-top: -4px; border 0px;',
                                labelSeparator: '',
                                xtype:'textarea',
                                name: 'description',
                                hideLabel: true,
                                grow: false,
                                preventScrollbars:false,
                                anchor:'100% 100%',
                                emptyText: this.app.i18n._('Enter description'),
                                requiredGrant: 'editGrant'                           
                            }]
                        }),
                        new Tine.widgets.activities.ActivitiesPanel({
                            app: 'Calendar',
                            showAddNoteForm: false,
                            border: false,
                            bodyStyle: 'border:1px solid #B5B8C8;'
                        }),
                        new Tine.widgets.tags.TagPanel({
                            app: 'Calendar',
                            border: false,
                            bodyStyle: 'border:1px solid #B5B8C8;'
                        })
                    ]
                },{
                    // activities and tags
                    region: 'south',
                    layout: 'form',
                    height: 150,
                    split: true,
                    margins: '0 5 0 5',
                    border: true,                    
                    collapsible: true,
                    collapseMode: 'mini',  
                    collapsed: true,                    
                    header: false,
                    items: [Tine.Tinebase.registry.get('userContact').id == this.record.get('organizer').id ? this.attachmentGrid : {html:"&nbsp;"}]
                }]
            }, new Tine.widgets.activities.ActivitiesTabPanel({
                app: this.appName,
                record_id: (this.record) ? this.record.id : '',
                record_model: this.appName + '_Model_' + this.recordClass.getMeta('modelName')
            })]
        };
    },
    /**
     * initToolbar
     */
    initToolbar: function() {
        var actionPrint = new Ext.Action({
            requiredGrant: 'readGrant',
            text: Tine.Tinebase.appMgr.get('Calendar').i18n._('Print Event'),
            handler: this.onPrint,
            iconCls: 'action_print',
            scope: this
         });
        var actionAttach = new Ext.Action({
            requiredGrant: 'readGrant',
            text: _('Add file'),
            handler: this.attachFiles.createDelegate(this),
            iconCls: 'action_add',
            scope: this,
            plugins: [{
                ptype: 'ux.browseplugin',
                multiple: true,
                dropElSelector: 'div[id=' + this.id + ']'
            }]
        });
        this.actionDelegate = new Ext.Action({
            requiredGrant: 'readGrant',
            text: Tine.Tinebase.appMgr.get('Calendar').i18n._('Delegate Attendance'),
            iconCls: 'action_delegate',
            handler: this.delegateAttendance.createDelegate(this),
            scope: this,
            disabled: true
        });

        this.tbarItems = [{xtype: 'widget-activitiesaddbutton'}, actionPrint, actionAttach, this.actionDelegate];
    },

    attachFiles: function(f,e) {
        this.attachmentGrid.onFilesSelect(f,e);
        this.attachmentGrid.ownerCt.expand();
    },

    /**
     * delegate attendance to a new user
     *
     */
    delegateAttendance: function() {
        this.delegateWindow = Tine.Calendar.AttendeeEditDialog.openWindow({
            listeners: {
                scope: this,
                update: function (eventJson) {
                    var o = Tine.Calendar.backend.recordReader({responseText: eventJson}).get('attendee')[0],
                        eventEditDialog = this;
                    o.dirty = true;
                    o.modified = {};
                    var freeBusyGrid = this.attendeeFreebusyGridPanel;
                    this.attendeeGridPanel.store.each(function(attender) {
                        if (Tine.Tinebase.registry.get('currentAccount').accountId == attender.get('user_id').account_id) {
                            var row = this.getView().getRow(this.store.indexOf(attender));
                            var idx = this.store.indexOf(attender);
                            // detect duplicate entry
                            // TODO detect duplicate emails, too
                            var isDuplicate = false;
                            this.store.each(function(att) {
                                if (o.user_id.id == att.getUserId()
                                        && o.user_type == att.get('user_type')
                                        && o != att) {
                                    row = this.getView().getRow(this.store.indexOf(att));
                                    isDuplicate = true;
                                    return false;
                                }
                            }, this);
                            if (! isDuplicate) {
                                this.store.remove(attender);
                                var record = new Tine.Calendar.Model.Attender(o, attender.id);
                                this.store.insert(idx, record);
                                freeBusyGrid.getStore().insert(idx, record);
                                row = this.getView().getRow(this.store.indexOf(record));
                                eventEditDialog.isDelegation = true;
                                eventEditDialog.onSaveAndClose();
                                // we need to close delegate window cause in some cases it keeps opened
                                if (eventEditDialog.delegateWindow) {
                                    eventEditDialog.delegateWindow.close.defer(200, eventEditDialog.delegateWindow);
                                }
                            }
                            Ext.fly(row).highlight();
                        }
                    }, this.attendeeGridPanel);
                }
            }
        });
    },

    initComponent: function() {
        this.changed = false;
        this.printRenderer = Tine.Calendar.Printer.EventViewRenderer;
        this.initToolbar();
        this.attendeeGridPanel = new Tine.Calendar.AttendeeGridPanel();
        this.attendeeFreebusyGridPanel = new Tine.Calendar.AttendeeInfinityGridPanel({
           tbar: [{
               xtype: 'label',
               html: Tine.Tinebase.appMgr.get('Calendar').i18n._('Granularity Time') + "&nbsp;"
           }, new  Ext.form.ComboBox({
            requiredGrant : 'editGrant',
            id: 'granularitytime',
            width: 80,
            listWidth: 80,
            triggerAction : 'all',
            hideLabel     : true,
            value         : 1,
            editable      : false,
            mode          : 'local',
            listeners     : {
                                scope: this,
                                select: function(combo, newValue) {
                                    switch(newValue.data.field1) {
                                        case 1:
                                            this.attendeeFreebusyGridPanel.timeGranularity = 60;
                                            this.attendeeFreebusyGridPanel.getView().refresh(false);
                                            this.attendeeFreebusyGridPanel.onRefresh();
                                            break;
                                        case 2:
                                            this.attendeeFreebusyGridPanel.timeGranularity = 120;
                                            this.attendeeFreebusyGridPanel.getView().refresh(false);
                                            this.attendeeFreebusyGridPanel.onRefresh();
                                            break;
                                        case 3:
                                            this.attendeeFreebusyGridPanel.timeGranularity = 360;
                                            this.attendeeFreebusyGridPanel.getView().refresh(false);
                                            this.attendeeFreebusyGridPanel.onRefresh();
                                            break;
                                        case 4:
                                            this.attendeeFreebusyGridPanel.timeGranularity = 720;

                                            this.attendeeFreebusyGridPanel.getView().refresh(false);
                                            this.attendeeFreebusyGridPanel.onRefresh();
                                            break;
                                    }
                                }
                            },
            store         : [
                [1,  Tine.Tinebase.appMgr.get('Calendar').i18n._('1h')  ],
                [2,  Tine.Tinebase.appMgr.get('Calendar').i18n._('2hs') ],
                [3,  Tine.Tinebase.appMgr.get('Calendar').i18n._('6hs')  ],
                [4,  Tine.Tinebase.appMgr.get('Calendar').i18n._('12hs') ]
            ]
           }),'->',
           {
               xtype: 'button',
               id: 'dbutton',
               text: Tine.Tinebase.appMgr.get('Calendar').i18n._('Original date'),
               listeners     : {
                                  scope: this,
                                  click: this.onDefaultButton
                               }
           },
           {
               xtype: 'button',
               id: 'pbutton',
               text: Tine.Tinebase.appMgr.get('Calendar').i18n._('Previus free date'),
               listeners     : {
                                  scope: this,
                                  click: this.onPreviusButton
                               }
           },
           {
               xtype: 'button',
               id: 'nbutton',
               text: Tine.Tinebase.appMgr.get('Calendar').i18n._('Next free date'),
               listeners     : {
                                  scope: this,
                                  click: this.onNextButton
                               }
           }],
           store : this.attendeeGridPanel.getStore(),
           startDate : Ext.decode(this.record).dtstart
        });

        // auto location
        this.attendeeGridPanel.on('afteredit', function(o) {
            if (o.field == 'user_id'
                && o.record.get('user_type') == 'resource'
                && o.record.get('user_id')
                && o.record.get('user_id').is_location
            ) {
                this.getForm().findField('location').setValue(
                    this.attendeeGridPanel.renderAttenderResourceName(o.record.get('user_id'))
                );
            }
            if (o.value) {
                this.attendeeFreebusyGridPanel.getStore().insert(this.attendeeFreebusyGridPanel.getStore().getCount() -1 ,o.record);
                this.attendeeFreebusyGridPanel.getView().refresh(false);
                this.attendeeFreebusyGridPanel.eventsStore.load({add:true});
            }
        }, this);
        var removed = 0;
        this.attendeeGridPanel.getStore().on('remove',function(store,record,target) {
            if(!removed) {
                removed=1;
                this.attendeeFreebusyGridPanel.getStore().removeAt(target);
            } else {
                removed=0;
            }
        }, this);
        // auto location
        this.attendeeFreebusyGridPanel.on('afteredit', function(o) {
            if (o.field == 'user_id'
                && o.record.get('user_type') == 'resource'
                && o.record.get('user_id')
                && o.record.get('user_id').is_location
            ) {
                this.getForm().findField('location').setValue(
                    this.attendeeFreebusyGridPanel.renderAttenderResourceName(o.record.get('user_id'))
                );
            }
            if (o.value) {
                this.attendeeGridPanel.getStore().insert(this.attendeeGridPanel.getStore().getCount() -1 ,o.record);
            }
        }, this);
        this.attendeeFreebusyGridPanel.getStore().on('remove',function(store,record,target) {
            if(!removed) {
                removed=1;
                this.attendeeGridPanel.getStore().removeAt(target);
            } else {
                removed=0;
            }
        }, this);
        var responseData = Ext.decode(this.record);
        this.attendeeFreebusyGridPanel.dtStart = responseData.dtstart;
        this.attendeeFreebusyGridPanel.dtEnd = responseData.dtend;
        this.attendeeFreebusyGridPanel.isAllDayEvent = responseData.is_all_day_event;
        this.attendeeFreebusyGridPanel.timeGranularity = 60;
        if(!Ext.isDate(this.attendeeFreebusyGridPanel.dtStart)){
               this.attendeeFreebusyGridPanel.dtStart = new Date.parseDate(this.attendeeFreebusyGridPanel.dtStart,"Y-m-d H:i:s");
           }
        if(!Ext.isDate(this.attendeeFreebusyGridPanel.dtEnd)){
               this.attendeeFreebusyGridPanel.dtEnd = new Date.parseDate(this.attendeeFreebusyGridPanel.dtEnd,"Y-m-d H:i:s");
        }
        if (this.attendeeFreebusyGridPanel.dtStart.add(Date.MINUTE,-this.attendeeFreebusyGridPanel.timeGranularity) < this.attendeeFreebusyGridPanel.startDayTime) {
            this.attendeeFreebusyGridPanel.topToolbar.items.get('pbutton').disable();
        }

        this.rrulePanel = new Tine.Calendar.RrulePanel({});
        this.alarmPanel = new Tine.widgets.dialog.AlarmPanel({});
        this.attendeeStore = this.attendeeGridPanel.getStore();


        // a combo with all attendee + origin/organizer
        this.perspectiveCombo = new Tine.Calendar.PerspectiveCombo({
            editDialog: this
        });
        Tine.Calendar.EventEditDialog.superclass.initComponent.call(this);

        this.addAttendee();

        this.actionDelegate.setDisabled(
            (this.record.get('organizer') != undefined
                && this.record.get('organizer').account_id == Tine.Tinebase.registry.get('currentAccount').accountId)
            || !this.record.getMyAttenderRecord()
        );
    },

    /**
     * if this addRelations is set, iterate and create attendee
     */
    addAttendee: function() {
        var attendee = this.record.get('attendee');
        var attendee = Ext.isArray(attendee) ? attendee : [];

        if (Ext.isArray(this.plugins)) {
            for (var index = 0; index < this.plugins.length; index++) {
                if (this.plugins[index].hasOwnProperty('addRelations')) {

                    var config = this.plugins[index].hasOwnProperty('relationConfig') ? this.plugins[index].relationConfig : {};

                    for (var index2 = 0; index2 < this.plugins[index].addRelations.length; index2++) {
                        var item = this.plugins[index].addRelations[index2];
                        var attender = Ext.apply({
                            user_type: 'user',
                            role: 'REQ',
                            quantity: 1,
                            status: 'NEEDS-ACTION',
                            user_id: item
                        }, config);

                        // don't add own attender, it is added automatically
                        if (Tine.Tinebase.registry.get('currentAccount').accountId != item.account_id) {
                            attendee.push(attender);
                        }
                    }
                }
            }
        }

        this.record.set('attendee', attendee);
    },

    /**
     *  handle previus button
     *
     */
     onPreviusButton: function(button, event) {
         if(this.attendeeFreebusyGridPanel.previusButton(this.attendeeFreebusyGridPanel.dtStart.add(Date.MINUTE,-this.attendeeFreebusyGridPanel.timeGranularity),this.attendeeFreebusyGridPanel.dtEnd.add(Date.MINUTE,-this.attendeeFreebusyGridPanel.timeGranularity))) {
             var dtStartField = this.getForm().findField('dtstart');
             var dtEndField = this.getForm().findField('dtend');
             //this.attendeeFreebusyGridPanel.getView().refresh(false);
             this.attendeeFreebusyGridPanel.updateEventTime();
             if (this.attendeeFreebusyGridPanel.dtStart.add(Date.MINUTE,-this.attendeeFreebusyGridPanel.timeGranularity).getTime() < this.attendeeFreebusyGridPanel.startDate.getTime()) {
                 this.attendeeFreebusyGridPanel.topToolbar.items.get('pbutton').disable();
             }
             Ext.each(this.attendeeFreebusyGridPanel.el.select('.freebusy-col').elements, function(row) {
                 Ext.get(row).scrollTo("l",this.attendeeFreebusyGridPanel.scrollTimePosition, true);
             }, this);
             if(this.attendeeFreebusyGridPanel.topToolbar.items.get('nbutton').disabled) {
                 this.attendeeFreebusyGridPanel.topToolbar.items.get('nbutton').enable();
             }
             dtStartField.setValue(this.attendeeFreebusyGridPanel.dtStart);
             dtEndField.setValue(this.attendeeFreebusyGridPanel.dtEnd);
          } else {
             this.attendeeFreebusyGridPanel.dtStart = this.getForm().findField('dtstart').getValue();
             this.attendeeFreebusyGridPanel.dtEnd = this.getForm().findField('dtend').getValue();
             Ext.Msg.alert('Status', 'No previus date found for this attendees.');
          }
     },

     /**
     *  handle next button
     *
     */
     onNextButton: function(button, event) {
         if(this.attendeeFreebusyGridPanel.nextButton(this.attendeeFreebusyGridPanel.dtStart.add(Date.MINUTE,this.attendeeFreebusyGridPanel.timeGranularity),this.attendeeFreebusyGridPanel.dtEnd.add(Date.MINUTE,this.attendeeFreebusyGridPanel.timeGranularity))) {
             var dtStartField = this.getForm().findField('dtstart');
             var dtEndField = this.getForm().findField('dtend');
             //this.attendeeFreebusyGridPanel.getView().refresh(false);
             this.attendeeFreebusyGridPanel.updateEventTime();
             if (this.attendeeFreebusyGridPanel.dtEnd.add(Date.MINUTE,this.attendeeFreebusyGridPanel.timeGranularity).getTime() > this.attendeeFreebusyGridPanel.startDate.add(Date.DAY,this.attendeeFreebusyGridPanel.numOfDays).getTime()) {
                 this.attendeeFreebusyGridPanel.topToolbar.items.get('nbutton').disable();
             }
             Ext.each(this.attendeeFreebusyGridPanel.el.select('.freebusy-col').elements, function(row) {
                 Ext.get(row).scrollTo("l",this.attendeeFreebusyGridPanel.scrollTimePosition, true);
             }, this);
             if(this.attendeeFreebusyGridPanel.topToolbar.items.get('pbutton').disabled) {
                 this.attendeeFreebusyGridPanel.topToolbar.items.get('pbutton').enable();
             }
             dtStartField.setValue(this.attendeeFreebusyGridPanel.dtStart);
             dtEndField.setValue(this.attendeeFreebusyGridPanel.dtEnd);
         } else {
             this.attendeeFreebusyGridPanel.dtStart = this.getForm().findField('dtstart').getValue();
             this.attendeeFreebusyGridPanel.dtEnd = this.getForm().findField('dtend').getValue();
             Ext.Msg.alert('Status', 'No next date found for this attendees.');
         }
     },

     /**
      * return to original data
      *
      */
     onDefaultButton: function(button, event) {
         var dtStartField = this.getForm().findField('dtstart');
         var dtEndField = this.getForm().findField('dtend');
         var diff = this.attendeeFreebusyGridPanel.dtStart.getTime() - this.record.data.dtstart.getTime();
         this.attendeeFreebusyGridPanel.dtStart = this.record.data.dtstart;
         this.attendeeFreebusyGridPanel.dtEnd = this.record.data.dtend;
         dtStartField.setValue(this.attendeeFreebusyGridPanel.dtStart);
         dtEndField.setValue(this.attendeeFreebusyGridPanel.dtEnd);
         this.getForm().findField('is_all_day_event').setValue( this.record.data.is_all_day_event);
         this.attendeeFreebusyGridPanel.startDate = dtStartField.getValue();
         this.attendeeFreebusyGridPanel.getView().refresh(false);
         if ( diff > 86400000){
            this.attendeeFreebusyGridPanel.eventsStore.load({});
         } else {
            this.attendeeFreebusyGridPanel.onRefresh();
         }
     },

    /**
     * confirm notification for organizer.
     *
     */

    handleNotification: function(button,event){
        var app = Tine.Tinebase.appMgr.get('Calendar');
        var prefs = app.getRegistry().get('preferences'),
            ask_for_notification = prefs.get('sendnotificationsforattendes'),
            user = Tine.Tinebase.registry.get('currentAccount').contact_id,
            organizerId = this.record.data.organizer.id,
            send = false;
        if ( (this.attendeeStore.getCount() > 2)
            || (this.attendeeStore.getCount() == 2 && user != this.attendeeStore.data.items[0].data.user_id.id) ) 
        {
            var send = true;
        }
        if (ask_for_notification == '60' && organizerId == user && send) {
            // NOTIFICATION_LEVEL_ATTENDEES_ASK (Ask me before send notification)
            Ext.MessageBox.confirm(_('Confirm'), app.i18n._('Do you want to send a notification to attendees of event?'), function(btn, text) {
                if (btn == 'yes') {
                    this.record.set('send', true);
                } else {
                    this.record.set('send', false);
                }
                this.onApplyChanges(button, event, true);
                this.fireEvent('saveAndClose', this, this.record);
            }, this);
        } else {
            if ( (ask_for_notification == '70' && organizerId == user && send)
                || this.isDelegation)
            {
                // NOTIFICATION_LEVEL_ATTENDEES_AUTOMATIC (Send notification automatically) 
                // OR attendee is delegating
                this.record.set('send', true);
            } else {
                // NOTIFICATION_LEVEL_ATTENDEES_NONE (Never send notification)
                this.record.set('send', false);
            }
            this.onApplyChanges(button, event, true);
            this.fireEvent('saveAndClose', this, this.record);
        }
    },

    /**
     * checks if form data is valid
     * 
     * @return {Boolean}
     */
    isValid: function() {
        this.validationErrorMessage = false;
        var isValid = this.validateDtStart() && this.validateDtEnd();
        
        if (this.attachmentGrid.isUploading()) {
            isValid = false;
            this.validationErrorMessage = this.app.i18n._('Files are still uploading.');
            return isValid;
        } 

        if (! this.rrulePanel.isValid()) {
            isValid = false;
            this.rrulePanel.ownerCt.setActiveTab(this.rrulePanel);
        }
        
        return isValid && Tine.Calendar.EventEditDialog.superclass.isValid.apply(this);
    },
     
    onAllDayChange: function(checkbox, isChecked) {
        var dtStartField = this.getForm().findField('dtstart');
        var dtEndField = this.getForm().findField('dtend');
        dtStartField.setDisabled(isChecked, 'time');
        dtEndField.setDisabled(isChecked, 'time');
        
        if (isChecked) {
            dtStartField.clearTime();
            var dtend = dtEndField.getValue();
            if (Ext.isDate(dtend) && dtend.format('H:i:s') != '23:59:59') {
                //dtEndField.setValue(dtend.clearTime(true).add(Date.HOUR, 24).add(Date.SECOND, -1));
		dtend = dtend.clearTime(true);
		if(dtend.getHours() == 01){
		    dtEndField.setValue(dtend.add(Date.HOUR, 23).add(Date.SECOND, -1));
		} else {
		    dtEndField.setValue(dtend.add(Date.HOUR, 24).add(Date.SECOND, -1));
		}
            }
            
        } else {
            dtStartField.setValue(this.record.data.dtstart);
            dtEndField.setValue(this.record.data.dtend);
            this.attendeeFreebusyGridPanel.startDate = dtStartField.getValue();
        }
        if(dtEndField.getValue().getHours() > this.attendeeFreebusyGridPanel.endDayTime){
           var freeBusyDtend = dtEndField.getValue();
           freeBusyDtend.clearTime(true);
           freeBusyDtend.setHours(this.attendeeFreebusyGridPanel.endDayTime);
           freeBusyDtend.setMinutes('00');
           this.attendeeFreebusyGridPanel.dtEnd = freeBusyDtend;
        } else {
           this.attendeeFreebusyGridPanel.dtEnd = dtEndField.getValue();
        }
        if(dtStartField.getValue().getHours() < this.attendeeFreebusyGridPanel.startDayTime){
           var freeBusyDtstart = dtStartField.getValue();
           freeBusyDtstart.setHours(this.attendeeFreebusyGridPanel.startDayTime);
           this.attendeeFreebusyGridPanel.dtStart = freeBusyDtstart;
        } else {
           this.attendeeFreebusyGridPanel.dtStart = dtStartField.getValue();
        }
        this.attendeeFreebusyGridPanel.getView().refresh(false);
        this.attendeeFreebusyGridPanel.onRefresh();
    },
    
    onDirty: function(field , newValue, oldValue){
         this.changed = true;
    },

    onDtEndChange: function(dtEndField, newValue, oldValue) {
        this.validateDtEnd();
        if(dtEndField.getValue().getHours() > this.attendeeFreebusyGridPanel.endDayTime){
           var freeBusyDtend = dtEndField.getValue();
           freeBusyDtend.clearTime(true);
           freeBusyDtend.setHours(this.attendeeFreebusyGridPanel.endDayTime);
           freeBusyDtend.setMinutes('00');
           this.attendeeFreebusyGridPanel.dtEnd = freeBusyDtend;
        } else {
           this.attendeeFreebusyGridPanel.dtEnd = dtEndField.getValue();
        }
        this.attendeeFreebusyGridPanel.getView().refresh(false);
        this.attendeeFreebusyGridPanel.onRefresh();
    },
    
    onDtStartChange: function(dtStartField, newValue, oldValue) {
        if (Ext.isDate(newValue) && Ext.isDate(oldValue)) {
            var diff = newValue.getTime() - oldValue.getTime();
            var dtEndField = this.getForm().findField('dtend');
            var dtEnd = dtEndField.getValue();
            if (Ext.isDate(dtEnd)) {
                dtEndField.setValue(dtEnd.add(Date.MILLI, diff));
            }
        }
        if(dtEndField.getValue().getHours() > this.attendeeFreebusyGridPanel.endDayTime){
           var freeBusyDtend = dtEndField.getValue();
           freeBusyDtend.clearTime(true);
           freeBusyDtend.setHours(this.attendeeFreebusyGridPanel.endDayTime);
           freeBusyDtend.setMinutes('00');
           this.attendeeFreebusyGridPanel.dtEnd = freeBusyDtend;
        } else {
           this.attendeeFreebusyGridPanel.dtEnd = dtEndField.getValue();
        }
        if(dtStartField.getValue().getHours() < this.attendeeFreebusyGridPanel.startDayTime){
           var freeBusyDtstart = dtStartField.getValue();
           freeBusyDtstart.setHours(this.attendeeFreebusyGridPanel.startDayTime);
           freeBusyDtstart.setMinutes('00');
           this.attendeeFreebusyGridPanel.dtStart = freeBusyDtstart;
        } else {
           this.attendeeFreebusyGridPanel.dtStart = dtStartField.getValue();
        }
        this.attendeeFreebusyGridPanel.startDate = dtStartField.getValue();
        this.attendeeFreebusyGridPanel.getView().refresh(false);
        if ( diff > 86400000){
           this.attendeeFreebusyGridPanel.eventsStore.load({});
        } else {
           this.attendeeFreebusyGridPanel.onRefresh();
        }
    },
    
    /**
     * copy record
     * 
     * TODO change attender status?
     */
    doCopyRecord: function() {
        Tine.Calendar.EventEditDialog.superclass.doCopyRecord.call(this);
        
        // remove attender ids
        Ext.each(this.record.data.attendee, function(attender) {
            delete attender.id;
        }, this);
        
        // Calendar is the only app with record based grants -> user gets edit grant for all fields when copying
        this.record.set('editGrant', true);
        
        Tine.log.debug('Tine.Calendar.EventEditDialog::doCopyRecord() -> record:');
        Tine.log.debug(this.record);
    },

    /**
     * is called after all subpanels have been loaded
     */
    onAfterRecordLoad: function() {
        Tine.Calendar.EventEditDialog.superclass.onAfterRecordLoad.call(this);
        
        this.attendeeGridPanel.onRecordLoad(this.record);
        this.attendeeFreebusyGridPanel.onRecordLoad(this.record);
        this.rrulePanel.onRecordLoad(this.record);
        this.alarmPanel.onRecordLoad(this.record);
        
        // apply grants
        if (! this.record.get('editGrant')) {
            this.getForm().items.each(function(f){
                if(f.isFormField && f.requiredGrant !== undefined){
                    f.setReadOnly(! this.record.get(f.requiredGrant));
                }
            }, this);
        }
        
        this.perspectiveCombo.loadPerspective();
        // disable container selection combo if user has no right to edit
        this.containerSelect.setDisabled.defer(20, this.containerSelect, [(! this.record.get('editGrant'))]);
        
        if (this.record.id) { // if is editing a event already created
            var allDayEventField = this.getForm().findField('is_all_day_event');
            if(allDayEventField.getValue()) {
            	this.onAllDayChange(allDayEventField, allDayEventField.getValue());
            }
        }
        
    	var timeZone = this.getForm().findField('originator_tz');
    	if(! timeZone.getValue()) {
    		timeZone.setValue(Tine.Tinebase.registry.get('timeZone'));
    	}

        if (this.delegationAutoStart) {
            this.delegateAttendance();
        }
    },
    
    onRecordUpdate: function() {       
        this.record.data.attachments = [];
        this.attachmentGrid.store.each(function(attachment) {
            this.record.data.attachments.push(Ext.ux.file.Upload.file.getFileData(attachment));
        }, this);          
        var _container_id = this.record.get('container_id'); // save container_id
        Tine.Calendar.EventEditDialog.superclass.onRecordUpdate.apply(this, arguments);
        var _new_container_id = this.record.get('container_id');
        if (!(_container_id instanceof Array) && (_new_container_id==='')) {
            // set container_id if reset by superclass
            // the container_id is needed for delegation
            this.record.set('container_id',_container_id);
        }
        else if (!Ext.isObject(_new_container_id)) {
            // retrieve full object from containerSelect's store
            _container_id = this.containerSelect.getStore().getById(_new_container_id);
            this.record.set('container_id',_container_id.data);
        }
        this.attendeeGridPanel.onRecordUpdate(this.record);
        this.attendeeFreebusyGridPanel.onRecordUpdate(this.record);
        this.rrulePanel.onRecordUpdate(this.record);
        this.alarmPanel.onRecordUpdate(this.record);
        this.perspectiveCombo.updatePerspective();
    },

    setTabHeight: function() {
        var eventTab = this.items.first().items.first();
        var centerPanel = eventTab.items.first();
        var tabPanel = centerPanel.items.last();
        tabPanel.setHeight(centerPanel.getEl().getBottom() - tabPanel.getEl().getTop());
    },
    
    validateDtEnd: function() {
        var dtStart = this.getForm().findField('dtstart').getValue();
        
        var dtEndField = this.getForm().findField('dtend');
        var dtEnd = dtEndField.getValue();
        
        if (! Ext.isDate(dtEnd)) {
            dtEndField.markInvalid(this.app.i18n._('End date is not valid'));
            return false;
        } else if (Ext.isDate(dtStart) && dtEnd.getTime() - dtStart.getTime() <= 0) {
            dtEndField.markInvalid(this.app.i18n._('End date must be after start date'));
            return false;
        } else {
            dtEndField.clearInvalid();
            return true;
        }
    },
    
    validateDtStart: function() {
        var dtStartField = this.getForm().findField('dtstart');
        var dtStart = dtStartField.getValue();
        
        if (! Ext.isDate(dtStart)) {
            dtStartField.markInvalid(this.app.i18n._('Start date is not valid'));
            return false;
        } else {
            dtStartField.clearInvalid();
            return true;
        }
        
    },
    
    /**
     * is called from onApplyChanges
     * @param {Boolean} closeWindow
     */
    doApplyChanges: function(closeWindow) {
        this.onRecordUpdate();
        if(this.isValid()) {
            this.fireEvent('update', Ext.util.JSON.encode(this.record.data));
            this.onAfterApplyChanges(closeWindow);
        } else {
            this.saving = false;
            this.loadMask.hide();
            Ext.MessageBox.alert(_('Errors'), this.getValidationErrorMessage());
        }
    }
});

/**
 * Opens a new event edit dialog window
 * 
 * @return {Ext.ux.Window}
 */
Tine.Calendar.EventEditDialog.openWindow = function (config) {
    // record is JSON encoded here...
    var id = config.recordId ? config.recordId : 0;
    var window = Tine.WindowFactory.getWindow({
        width: 800,
        height: 600,
        name: Tine.Calendar.EventEditDialog.prototype.windowNamePrefix + id,
        contentPanelConstructor: 'Tine.Calendar.EventEditDialog',
        contentPanelConstructorConfig: config
    });
    return window;
};
