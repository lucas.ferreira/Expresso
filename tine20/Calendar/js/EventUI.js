/* 
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @note: lot to do here, i just started to move stuff from views here
 */
 
Ext.ns('Tine.Calendar');

Tine.Calendar.EventUI = function(event) {
    this.event = event;
    this.domIds = [];
    this.app = Tine.Tinebase.appMgr.get('Calendar');
    this.init();
};

Tine.Calendar.EventUI.prototype = {
    addClass: function(cls) {
        Ext.each(this.getEls(), function(el){
            el.addClass(cls);
        });
    },
    
    blur: function() {
        Ext.each(this.getEls(), function(el){
            el.blur();
        });
    },
    
    clearDirty: function() {
        Ext.each(this.getEls(), function(el) {
            el.setOpacity(1, 1);
        });
    },
    
    focus: function() {
        Ext.each(this.getEls(), function(el){
            el.focus();
        });
    },
    
    /**
     * returns events dom
     * @return {Array} of Ext.Element
     */
    getEls: function() {
        var domEls = [];
        for (var i=0; i<this.domIds.length; i++) {
            var el = Ext.get(this.domIds[i]);
            if (el) {
                domEls.push(el);
            }
        }
        return domEls;
    },
    
    init: function() {
        // shortcut
        //this.colMgr = Tine.Calendar.colorMgr;
    },
    
    markDirty: function() {
        Ext.each(this.getEls(), function(el) {
            el.setOpacity(0.5, 1);
        });
    },
    
    markOutOfFilter: function() {
        Ext.each(this.getEls(), function(el) {
            el.setOpacity(0.5, 0);
            el.setStyle({'background-color': '#aaa', 'border-color': '#888'});
            Ext.DomHelper.applyStyles(el.dom.firstChild, {'background-color': '#888'});
            Ext.DomHelper.applyStyles(el.dom.firstChild.firstChild, {'background-color': '#888'});
        });
    },
    
    onSelectedChange: function(state){
        if(state){
            //this.focus();
            this.addClass('cal-event-active');
            this.setStyle({'z-index': 1000});
            
        }else{
            //this.blur();
            this.removeClass('cal-event-active');
            this.setStyle({'z-index': 100});
        }
    },
    
    /**
     * removes a event from the dom
     */
    remove: function() {
        var eventEls = this.getEls();
        for (var i=0; i<eventEls.length; i++) {
            if (eventEls[i] && typeof eventEls[i].remove == 'function') {
                eventEls[i].remove();
            }
        }
        if (this.resizeable) {
            this.resizeable.destroy();
            this.resizeable = null;
        }
        this.domIds = [];
    },
    
    removeClass: function(cls) {
        Ext.each(this.getEls(), function(el){
            el.removeClass(cls);
        });
    },
    
    render: function() {
        // do nothing
    },
    
    setOpacity: function(v) {
        Ext.each(this.getEls(), function(el){
            el.setStyle(v);
        });
    },
    
    setStyle: function(style) {
        Ext.each(this.getEls(), function(el){
            el.setStyle(style);
        });
    }
    
};



Tine.Calendar.DaysViewEventUI = Ext.extend(Tine.Calendar.EventUI, {
    
    clearDirty: function() {
        Tine.Calendar.DaysViewEventUI.superclass.clearDirty.call(this);
        
        Ext.each(this.getEls(), function(el) {
            el.setStyle({'border-style': 'solid'});
        });
    },
    /**
     * get diff of resizeable
     * 
     * @param {Ext.Resizeable} rz
     */
    getRzInfo: function(rz, width, height) {
        var rzInfo = {};
        
        var event = rz.event;
        var view = event.view;
        
        // NOTE proxy might be gone after resize
        var box = rz.proxy.getBox();
        var width = width ? width: box.width;
        var height =  height? height : box.height;
        
        var originalDuration = (event.get('dtend').getTime() - event.get('dtstart').getTime()) / Date.msMINUTE;
        
        if(event.get('is_all_day_event')) {
            var dayWidth = Ext.fly(view.wholeDayArea).getWidth() / view.numOfDays;
            rzInfo.diff = Math.round((width - rz.originalWidth) / dayWidth);
            
        } else {
            rzInfo.diff = Math.round((height - rz.originalHeight) * (view.timeGranularity / view.granularityUnitHeights));
            // neglegt diffs due to borders etc.
            rzInfo.diff = Math.round(rzInfo.diff/15) * 15;
        }
        rzInfo.duration = originalDuration + rzInfo.diff;
        
        if(event.get('is_all_day_event')) {
            rzInfo.dtend = event.get('dtend').add(Date.DAY, rzInfo.diff);
        } else {
            rzInfo.dtend = event.get('dtstart').add(Date.MINUTE, rzInfo.duration);
        }
        
        return rzInfo;
    },
    
    markDirty: function() {
        Tine.Calendar.DaysViewEventUI.superclass.markDirty.call(this);
        
        Ext.each(this.getEls(), function(el) {
            el.setStyle({'border-style': 'dashed'});
        });
    },
    
    getAttendees: function(event){
        var attendees ="";
        Ext.each(event.data.attendee, function(attender) {
            attendees += attender.user_id.n_fn + ", ";
        }, this);

        return attendees.substring(0,attendees.length -2 );
    },

    onSelectedChange: function(state){
        Tine.Calendar.DaysViewEventUI.superclass.onSelectedChange.call(this, state);
        if(state){
            this.addClass('cal-daysviewpanel-event-active');
            
        }else{
            this.removeClass('cal-daysviewpanel-event-active');
        }
    },
    
    render: function(view) {
        this.event.view = view;

        this.colorSet = Tine.Calendar.colorMgr.getColor(this.event);
        this.event.colorSet = this.colorSet;
        
        this.dtStart = this.event.get('dtstart');
        this.startColNum = view.getColumnNumber(this.dtStart);
        
        this.dtEnd = this.event.get('dtend');
        
        if (this.event.get('editGrant')) {
            this.extraCls = 'cal-daysviewpanel-event-editgrant';
        }
        
        this.extraCls += ' cal-status-' + this.event.get('status');
        
        // 00:00 in users timezone is a spechial case where the user expects
        // something like 24:00 and not 00:00
        if (this.dtEnd.format('H:i') == '00:00') {
            this.dtEnd = this.dtEnd.add(Date.MINUTE, -1);
        }
        this.endColNum = view.getColumnNumber(this.dtEnd);
        
        // skip dates not in our diplay range
        if (this.endColNum < 0 || this.startColNum > view.numOfDays-1) {
            return;
        }
        
        // compute status icons
        this.statusIcons = [];
        if (this.event.get('class') === 'PRIVATE') {
            this.statusIcons.push({
                status: 'private',
                text: this.app.i18n._('private classification')
            });
        }
        
        if (this.event.get('rrule')) {
            this.statusIcons.push({
                status: 'recur',
                text: this.app.i18n._('recurring event')
            });
        } else if (this.event.isRecurException()) {
            this.statusIcons.push({
                status: 'recurex',
                text: this.app.i18n._('recurring event exception')
            });
        }

        
        
        if (! Ext.isEmpty(this.event.get('alarms'))) {
            this.statusIcons.push({
                status: 'alarm',
                text: this.app.i18n._('has alarm')
            });
        }
        
        var myAttenderRecord = this.event.getMyAttenderRecord(),
            myAttenderStatusRecord = myAttenderRecord ? Tine.Tinebase.widgets.keyfield.StoreMgr.get('Calendar', 'attendeeStatus').getById(myAttenderRecord.get('status')) : null;
            
        if (myAttenderStatusRecord && myAttenderStatusRecord.get('system')) {
            this.statusIcons.push({
                status: myAttenderRecord.get('status'),
                text: myAttenderStatusRecord.get('i18nValue')
            });
        }
        
        var registry = this.event.get('is_all_day_event') ? view.parallelWholeDayEventsRegistry : view.parallelScrollerEventsRegistry;
        
        var position = registry.getPosition(this.event);
        var maxParallels = registry.getMaxParalles(this.dtStart, this.dtEnd);
        
        if (this.event.get('is_all_day_event')) {
            this.renderAllDayEvent(view, maxParallels, position,false);
        } else {
            this.renderScrollerEvent(view, maxParallels, position,false);
        }
        
        if (this.event.dirty) {
            // the event was selected before
            this.onSelectedChange(true);
        }
        
        if (this.event.outOfFilter) {
            this.markOutOfFilter();
        }
        
        this.rendered = true;
    },


    printEvent: function(view) {
        this.event.view = view;

        this.colorSet = Tine.Calendar.colorMgr.getColor(this.event);
        this.event.colorSet = this.colorSet;

        this.dtStart = this.event.get('dtstart');
        this.startColNum = view.getColumnNumber(this.dtStart);

        this.dtEnd = this.event.get('dtend');

        if (this.event.get('editGrant')) {
            this.extraCls = 'cal-daysviewpanel-event-editgrant';
        }

        this.extraCls += ' cal-status-' + this.event.get('status');

        // 00:00 in users timezone is a spechial case where the user expects
        // something like 24:00 and not 00:00
        if (this.dtEnd.format('H:i') == '00:00') {
            this.dtEnd = this.dtEnd.add(Date.MINUTE, -1);
        }
        this.endColNum = view.getColumnNumber(this.dtEnd);

        // compute status icons
        this.statusIcons = [];
        if (this.event.get('class') === 'PRIVATE') {
            this.statusIcons.push({
                status: 'private',
                text: this.app.i18n._('private classification')
            });
        }

        if (this.event.get('rrule')) {
            this.statusIcons.push({
                status: 'recur',
                text: this.app.i18n._('recurring event')
            });
        } else if (this.event.isRecurException()) {
            this.statusIcons.push({
                status: 'recurex',
                text: this.app.i18n._('recurring event exception')
            });
        }

        if (! Ext.isEmpty(this.event.get('alarms'))) {
            this.statusIcons.push({
                status: 'alarm',
                text: this.app.i18n._('has alarm')
            });
        }

        var myAttenderRecord = this.event.getMyAttenderRecord(),
            myAttenderStatusRecord = myAttenderRecord ? Tine.Tinebase.widgets.keyfield.StoreMgr.get('Calendar', 'attendeeStatus').getById(myAttenderRecord.get('status')) : null;

        if (myAttenderStatusRecord && myAttenderStatusRecord.get('system')) {
            this.statusIcons.push({
                status: myAttenderRecord.get('status'),
                text: myAttenderStatusRecord.get('i18nValue')
            });
        }

        var registry = this.event.get('is_all_day_event') ? view.parallelWholeDayEventsRegistry : view.parallelScrollerEventsRegistry;

        var position = registry.getPosition(this.event);
        var maxParallels = registry.getMaxParalles(this.dtStart, this.dtEnd);
        var allDay = [];
        var scrollerEvent = [];
        if (this.event.get('is_all_day_event')) {
            allDay["allDay"] = this.renderAllDayEvent(view, maxParallels, position,true);
            return  allDay;
        } else {
            scrollerEvent["scroller"] = this.renderScrollerEvent(view, maxParallels, position,true);
            return scrollerEvent;
        }
    },

    renderAllDayEvent: function(view, parallels, pos, print) {
        // lcocal COPY!

        // Number of days of view
        workdays=0;
        y=1;
        for(var j=0;j < view.numOfDays ; j++){
            g = view.work & y;
            if(g > 0){
                workdays++;
            }
            y <<= 1;
        }

        // Start column of view. First column is 0
        col=workdays;
        y=64;
        for(var j=(view.numOfDays-1) ;j >= this.dtStart.getDay(); j--){
            y >>= (view.numOfDays-1) - j;
            g = view.work & y;
            if(g > 0){
               col--;
            }
            y=64;
        }

        // End column of view
        colEnd=0;
        y=1;
        for(var j=0;j <= this.dtEnd.getDay() ; j++){
            y <<= j;
            g = view.work & y;
            if(g > 0){
               colEnd++;
            }
            y=1;
        }

        var extraCls = this.extraCls;
        var offsetWidth = Ext.fly(view.wholeDayArea).getWidth();

        // Assuming that a seven days week has the constant width of 100%
        // Vars left, width and right are the event position and width relative to week
        var width = Math.floor(1000 * (this.dtEnd.getTime() - this.dtStart.getTime()) / (view.numOfDays * Date.msDAY) -5) /10;
        var left = 100 * (this.dtStart.getTime() - view.startDate.getTime()) / (view.numOfDays * Date.msDAY);
        var right = left + width;

        var viewLeft = left;
        var viewWidth = width;

        if (left < 0) {
            // startDate in previous week
            col = 0;
            viewLeft = 0;
            viewWidth = left + width;
            extraCls = extraCls + ' cal-daysviewpanel-event-cropleft';
        } else {
            viewLeft = col * (100 / workdays);
        }

        if (right > 100) {
            // endDate in future week
            extraCls = extraCls + ' cal-daysviewpanel-event-cropright';
        } else {
            viewWidth = (colEnd - col) * (100 / workdays);
        }

        if(print){
            var eventEl = view.templates.wholeDayPrintEvent.applyTemplate({
                id: this.domIds[0],
                summary: this.event.get('summary'),
                startTime: this.dtStart.format('H:i'),
                extraCls: extraCls,
                color: this.colorSet.color,
                bgColor: this.colorSet.light,
                textColor: this.colorSet.text,
                zIndex: 100,
                width: 100  +'%',
                height: '15px',
                left:   viewLeft + '%',
                top: pos * 18 + 'px' //'1px'
            });
            return eventEl;
        } else {
            var domId = Ext.id() + '-event:' + this.event.get('id');
            this.domIds.push(domId);

            var eventEl = view.templates.wholeDayEvent.insertFirst(view.wholeDayArea, {
                id: domId,
                tagsHtml: Tine.Tinebase.common.tagsRenderer(this.event.get('tags')),
                summary: this.event.get('summary'),
                startTime: this.dtStart.format('H:i'),
                extraCls: extraCls,
                color: this.colorSet.color,
                bgColor: this.colorSet.light,
                textColor: this.colorSet.text,
                zIndex: 100,
                width: viewWidth  +'%',
                height: '15px',
                left: viewLeft + '%',
                top: pos * 18 + 'px',//'1px'
                statusIcons: this.statusIcons
            }, true);

            if (this.event.dirty) {
                eventEl.setStyle({'border-style': 'dashed'});
                eventEl.setOpacity(0.5);
            }

            if (! (this.endColNum > view.numOfDays) && this.event.get('editGrant')) {
                this.resizeable = new Ext.Resizable(eventEl, {
                    handles: 'e',
                    disableTrackOver: true,
                    dynamic: true,
                    //dynamic: !!this.event.isRangeAdd,
                    widthIncrement: Math.round(offsetWidth / view.numOfDays),
                    minWidth: Math.round(offsetWidth / view.numOfDays),
                    listeners: {
                        scope: view,
                        resize: view.onEventResize,
                        beforeresize: view.onBeforeEventResize
                    }
                });
            }
        }
        //console.log([eventEl.dom, parallels, pos])
    },
    
    renderScrollerEvent: function(view, parallels, pos, print) {
        var scrollerHeight = view.granularityUnitHeights * ((24 * 60)/view.timeGranularity);
        workdays=0;
        y=1;
        for(var j=0;j < view.numOfDays ; j++){
            g = view.work & y;
            if(g > 0){
                workdays++;
            }
            y <<= 1;
        }

        for (var currColNum=this.startColNum; currColNum<=this.endColNum; currColNum++) {
            
            // lcocal COPY!
            var extraCls = this.extraCls;
            
            if (currColNum < 0 || currColNum >= workdays) {
                continue;
            }
            if (parseInt(view.startDayTime) > this.dtStart.getHours() && print ){
                var top = 0;
                var correctHeight = view.getTimeOffset(this.dtStart,print);
                var height = this.startColNum == this.endColNum ? view.getTimeHeight(this.dtStart, this.dtEnd, print) : view.getTimeOffset(this.dtEnd, print);
                height = height + correctHeight;
            }else{
                var top = view.getTimeOffset(this.dtStart, print);
                var height = this.startColNum == this.endColNum ? view.getTimeHeight(this.dtStart, this.dtEnd, print) : view.getTimeOffset(this.dtEnd, print);
            }
            if (currColNum != this.startColNum) {
                top = 0;
                extraCls = extraCls + ' cal-daysviewpanel-event-croptop';
            }
            
            if (this.endColNum != currColNum) {
                height = view.getTimeHeight(this.dtStart, this.dtStart.add(Date.HOUR, parseInt(view.endDayTime) - parseInt(view.startDayTime)), print) - top;
                extraCls = extraCls + ' cal-daysviewpanel-event-cropbottom';
            }

            // minimal height
            if (height <= 12) {
                height = 12;
            }
            
            // minimal top
            if (top > scrollerHeight -12) {
                top = scrollerHeight -12;
            }
	    
	    if (!Tine.Tinebase.appMgr.get('Calendar').getMainScreen().getCenterPanel().rendered){
		continue;
	    }
	    
            if(print) {
                 var eventEl = view.templates.eventPrint.applyTemplate({
                    id: this.domIds[0],
                    summary: height >= 24 ? this.event.get('summary') : '',
                    tagsHtml: height >= 24 ? Tine.Tinebase.common.tagsRenderer(this.event.get('tags')) : '',
                    startTime: (height >= 24 && top <= scrollerHeight-24) ? this.dtStart.format('H:i') : this.dtStart.format('H:i') + ' ' +  this.event.get('summary'),
                    extraCls: extraCls,
                    color: this.colorSet.color,
                    bgColor: this.colorSet.light,
                    textColor: this.colorSet.text,
                    zIndex: 100,
                    attendees: height >= 36 ? this.getAttendees(this.event) : '',
                    description: height >= 72 && this.event.get('description') ? this.event.get('description') : '',
                    height: height + 'px',
                    left: Math.round(pos * 100 * 1/parallels) + '%',
                    width: Math.round((100 * 1/parallels) -1) + '%',
                    top: top + 'px'
                 });
                 return eventEl;
            } else {
                  var domId = Ext.id() + '-event:' + this.event.get('id');
                  this.domIds.push(domId);
                  var eventEl = view.templates.event.append(view.getDateColumnEl(currColNum), {
                      id: domId,
                      summary: height >= 24 ? this.event.get('summary') : '',
                      tagsHtml: height >= 24 ? Tine.Tinebase.common.tagsRenderer(this.event.get('tags')) : '',
                      startTime: (height >= 24 && top <= scrollerHeight-24) ? this.dtStart.format('H:i') : this.dtStart.format('H:i') + ' ' +  this.event.get('summary'),
                      extraCls: extraCls,
                      color: this.colorSet.color,
                      bgColor: this.colorSet.light,
                      textColor: this.colorSet.text,
                      zIndex: 100,
                      height: height + 'px',
                      left: Math.round(pos * 90 * 1/parallels) + '%',
                      width: Math.round(90 * 1/parallels) + '%',
                      top: top + 'px',
                      statusIcons: this.statusIcons
                  }, true);
                  if (this.event.dirty) {
                      eventEl.setStyle({'border-style': 'dashed'});
                      eventEl.setOpacity(0.5);
                  }
                  if (currColNum == this.endColNum && this.event.get('editGrant')) {
                      this.resizeable = new Ext.Resizable(eventEl, {
                          handles: 's',
                          disableTrackOver: true,
                          dynamic: true,
                          //dynamic: !!this.event.isRangeAdd,
                          heightIncrement: view.granularityUnitHeights/2,
                          listeners: {
                              scope: view,
                              resize: view.onEventResize,
                              beforeresize: view.onBeforeEventResize
                          }
                      });
                  }
            }
        }
    }
});

Tine.Calendar.FreebusyViewEventUI = Ext.extend(Tine.Calendar.EventUI, {
    /**
     * get diff of resizeable
     *
     * @param {Ext.Resizeable} rz
     */
    getRzInfo: function(rz, width, height) {
        var rzInfo = {};
        var event = rz.event;
        var view = event.view;
        // NOTE proxy might be gone after resize
        var box = rz.proxy.getBox();
        var width = width ? width: box.width;
        var height =  height? height : box.height;
        var originalDuration = (event.get('dtend').getTime() - event.get('dtstart').getTime()) / Date.msMINUTE;
        if(event.get('is_all_day_event')) {
            var dayWidth = Ext.fly(view.wholeDayArea).getWidth() / view.numOfDays;
            rzInfo.diff = Math.round((width - rz.originalWidth) / dayWidth);
        } else {
            rzInfo.diff = Math.round((height - rz.originalHeight) * (view.timeGranularity / view.granularityUnitHeights));
            // neglegt diffs due to borders etc.
            rzInfo.diff = Math.round(rzInfo.diff/15) * 15;
        }
        rzInfo.duration = originalDuration + rzInfo.diff;
        if(event.get('is_all_day_event')) {
            rzInfo.dtend = event.get('dtend').add(Date.DAY, rzInfo.diff);
        } else {
            rzInfo.dtend = event.get('dtstart').add(Date.MINUTE, rzInfo.duration);
        }
        return rzInfo;
    },

    isAttendee: function(userAccountId){
        var attendee = false;
        Ext.each(this.event.data.attendee, function(attender) {
            if( attender != "") {
                if (attender.user_id.account_id == userAccountId) {
                    attendee = true;
                }
            }
        }, this);

        return attendee;
    },

    render: function(grid) {
        var row = [];
        this.grid = grid;
        this.grid.store.each(function(attender) {
            if(attender.getUserAccountId()){
                if (this.event.get('container_id').owner_id == attender.getUserAccountId()) {
                    row.push(this.grid.store.indexOf(attender));
                } else if ( this.isAttendee(attender.getUserAccountId())) {
                    row.push(this.grid.store.indexOf(attender));
                }
            }
        }, this);

        var baseDate = this.grid.startDate.clone();
        this.dtStart = this.event.get('dtstart');
        this.dtEnd = this.event.get('dtend');
        if(this.dtEnd.getHours() > this.grid.endDayTime){
            var freeBusyDtend = this.dtEnd;
            freeBusyDtend.clearTime(true);
            freeBusyDtend.setHours(this.grid.endDayTime);
            freeBusyDtend.setMinutes('00');
            this.dtEnd = freeBusyDtend;
        }
        if(this.dtStart.getHours() < this.grid.startDayTime){
            var freeBusyDtstart = this.dtStart;
            freeBusyDtstart.setHours(this.grid.startDayTime);
            freeBusyDtstart.setMinutes('00');
            this.dtStart = freeBusyDtstart;
        }

        var startDay = Math.floor((this.dtStart.getTime() - this.grid.startDate.getTime())/(1000*60*60*24));
        var endDay = Math.floor((this.dtEnd.getTime() - this.grid.startDate.getTime())/(1000*60*60*24));
        if (baseDate.add(Date.DAY, this.grid.numOfDays).getTime() < this.dtEnd.getTime()) {
            endDay = Math.floor((baseDate.add(Date.DAY, this.grid.numOfDays -1).getTime()  - this.grid.startDate.getTime())/(1000*60*60*24));
        }
        if (this.event.get('is_all_day_event') && startDay == endDay) {
            var widht = this.grid.timeScale.getCount() * (this.grid.granularityUnitWidht+1.11);
            var startPosition = (this.grid.timeScale.getCount() * startDay * (this.grid.granularityUnitWidht+1.11));
            this.renderEvent(grid, widht, startPosition,row);
        } else {
            for (var j=startDay;j<= endDay;j++) {
                if ( startDay != endDay && j != endDay && j != startDay ){ // middle days
                    var widht = this.grid.timeScale.getCount() * (this.grid.granularityUnitWidht+1.11);
                    var startPosition = (this.grid.timeScale.getCount() * j * (this.grid.granularityUnitWidht+1.11));
                } else if ( startDay != endDay && j == endDay ) { // last Day
                    var riseTimeDate = Date.parseDate(this.dtEnd.format('Y-m-d') + ' ' + this.grid.startDayTime  + ':00:00', Date.patterns.ISO8601Long);
                    var widht = (this.grid.granularityUnitWidht / this.grid.timeGranularity) * ((this.dtEnd.getTime() - riseTimeDate.getTime())/60000);
                    var startPosition = (this.grid.timeScale.getCount() * endDay * (this.grid.granularityUnitWidht+1.11));
                } else {
                    if ( startDay != endDay && j == startDay ) { // first day
                        var setTimeDate = Date.parseDate(this.dtStart.format('Y-m-d') + ' ' + this.grid.endDayTime  + ':00:00', Date.patterns.ISO8601Long);
                        var widht = ((this.grid.granularityUnitWidht +1)/ this.grid.timeGranularity) * ((setTimeDate.getTime() - this.dtStart.getTime())/60000);
                    } else {
                        var widht = ((this.grid.granularityUnitWidht +1)/ this.grid.timeGranularity) * ((this.dtEnd.getTime() - this.dtStart.getTime())/60000);
                    }
                    for (var i = 0; i < this.grid.timeScale.getCount(); i++) {
                        var currentDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.grid.timeScale.data.items[i].data.time + ':00', Date.patterns.ISO8601Long);
                        var currentMin = ((currentDate.getHours() * 60) + currentDate.getMinutes())-(this.grid.timeGranularity/2);
                        if( i == (this.grid.timeScale.getCount()-1)){
                            var nextMin = ((currentDate.getHours() * 60) + currentDate.getMinutes())+(this.grid.timeGranularity/2);
                        } else {
                            var nextDate = Date.parseDate(baseDate.add(Date.DAY, 1).format('Y-m-d') + ' ' + this.grid.timeScale.data.items[i+1].data.time + ':00', Date.patterns.ISO8601Long);
                            var nextMin = ((nextDate.getHours() * 60) + nextDate.getMinutes())-(this.grid.timeGranularity/2);
                        }
                        var dtStartMin = (this.dtStart.getHours() * 60) + this.dtStart.getMinutes();
                        if( dtStartMin >= currentMin && dtStartMin < nextMin ) {
                            var startPosition = (((this.grid.granularityUnitWidht / this.grid.timeGranularity) * (dtStartMin - currentMin)) + (i * (this.grid.granularityUnitWidht+1.11))) + (this.grid.timeScale.getCount() * startDay * (this.grid.granularityUnitWidht+1.11));
                            var itemid = i + (this.grid.timeScale.getCount() * startDay);
                            break;
                        }
                    }
                }
                this.renderEvent(grid, widht, startPosition,row);
            }
        }
        this.rendered = true;
    },

    renderEvent: function(grid, eventWidht, startPosition,row) {
        for(var i=0;i<row.length;i++){
            var freebusyRow = Ext.get(grid.el.select('.freebusy-col-line').elements[row[i]]);
            var domId = Ext.id() + '-event:' + this.event.get('id');
            this.domIds.push(domId);
            var eventEl = grid.templates.event.append(freebusyRow, {
                      id: domId,
                      height: '18px',
                      left: startPosition + 'px',
                      width: eventWidht + 'px',
                      status: this.event.get('summary')
                  }, true);
        }
    },
});

Tine.Calendar.MonthViewEventUI = Ext.extend(Tine.Calendar.EventUI, {
    onSelectedChange: function(state){
        Tine.Calendar.MonthViewEventUI.superclass.onSelectedChange.call(this, state);
        if (state){
            this.addClass('cal-monthview-active');
            this.setStyle({
                'background-color': this.color,
                'color':            (this.colorSet) ? this.colorSet.text : '#000000'
            });
            
        } else {
            this.removeClass('cal-monthview-active');
            this.setStyle({
                'background-color': this.is_all_day_event ? this.bgColor : '',
                'color':            this.is_all_day_event ? '#000000' : this.color
            });
        }
    }
});
