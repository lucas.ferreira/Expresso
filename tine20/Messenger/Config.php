<?php
/**
 * @package     Messenger
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Messenger config class
 * 
 * @package     Messenger
 * @subpackage  Config
 */
class Messenger_Config extends Tinebase_Config_Abstract
{
    /**
     * DOMAIN
     */
    const DOMAIN = 'domain';

    /**
     * RESOURCE
     */
    const RESOURCE = 'resource';

    /**
     * FORMAT
     */
    const FORMAT = 'format';

    /**
     * SERVER_URL
     */
    const SERVER_URL = 'rtmfpServerUrl';

    /**
     * TEMP_FILES
     */
    const TEMP_FILES = 'tempFiles';

    /**
     * DISABLE_VIDEO_CHAT
     */
    const DISABLE_VIDEO_CHAT = 'disableVideoChat';

    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Definition::$_properties
     */
    protected static $_properties = array(
        self::DOMAIN => array(
                                    //_('Specify the Jabber domain')
            'label'                 => 'Specify the Jabber domain',
                                    //_('Specify the Jabber domain')
            'description'           => 'Specify the Jabber domain',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => '',
        ),
        self::RESOURCE => array(
                                    //_('Specify the resource for Jabber client')
            'label'                 => 'Specify the resource for Jabber client',
                                    //_('Specify the resource for Jabber client')
            'description'           => 'Specify the resource for Jabber client',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => '',
        ),
        self::FORMAT => array(
                                    //_('Login format')
            'label'                 => 'Login format',
                                    //_('Login format')
            'description'           => 'Login format',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => 'email',
        ),
        self::SERVER_URL => array(
                                    //_('Server Url')
            'label'                 => 'Server Url',
                                    //_('Server Url')
            'description'           => 'Server Url',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => '',
        ),
        self::TEMP_FILES => array(
                                    //_('Path for temp files')
            'label'                 => 'Path for temp files',
                                    //_('Path for temp files')
            'description'           => 'Path for temp files',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => '/tmp/',
        ),
        self::DISABLE_VIDEO_CHAT => array(
                                    //_('Disable Video Chat')
            'label'                 => 'Disable Video Chat',
                                    //_('Disable Video Chat')
            'description'           => 'Disable Video Chat',
            'type'                  => Tinebase_Config_Abstract::TYPE_BOOL,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => FALSE,
        ),

    );
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::$_appName
     */
    protected $_appName = 'Messenger';
    
    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Config
     */
    private static $_instance = NULL;
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {}
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __clone() {}
    
    /**
     * Returns instance of Tinebase_Config
     *
     * @return Tinebase_Config
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::getProperties()
     */
    public static function getProperties()
    {
        return self::$_properties;
    }
}