<?php

/**
 * Tine 2.0
 *
 * @package     Messenger
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */

class Messenger_Setup_Update_Release1 extends Setup_Update_Abstract
{
    /**
     * update to 1.1 - Change config key from Tinebase to Messenger
     * @return void
     */
    public function update_0()
    {
	
        
	$config = Tinebase_Config::getInstance()->get('messenger');
	if(!is_null($config)){
	    // create status config
	    $cb = new Tinebase_Backend_Sql(array(
		'modelName' => 'Tinebase_Model_Config', 
		'tableName' => 'config',
	    ));
	    
	    $config = $config->toArray();
	    $newConfig = 
		array(
		    "domain" => $config['messenger']['domain'] ? $config['messenger']['domain'] : '',
		    "resource" => $config['messenger']['resource'] ? $config['messenger']['resource'] : '',
		    "format" => $config['messenger']['format'] ? $config['messenger']['format'] : '',
		    "rtmfpServerUrl" => $config['messenger']['rtmfpServerUrl'] ? $config['messenger']['rtmfpServerUrl'] : '',
		    "tempFiles" => $config['messenger']['tempFiles'] ? $config['messenger']['tempFiles'] : ''
		);
	    
	     $cb->create(new Tinebase_Model_Config(array(
		'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Messenger')->getId(),
		'name'              => 'messenger',
		'value'             => json_encode($newConfig),
	    )));
	}
	
        $this->setApplicationVersion('Messenger', '1.1');
    }

    /**
     * update to 1.2 - Normalize config
     * @return void
     */
    public function update_1()
    {
        $config = Tinebase_Config::getInstance()->get('Messenger');

        if(!is_null($config)) {

            Tinebase_Config::getInstance()->delete('Messenger');

            $config = $config->toArray();
            if(isset($config['messenger'])) {

                if(Messenger_Config::isJson($config['messenger'])) {
                    $config['messenger'] = json_decode($config['messenger'], TRUE);
                }

                $properties = Messenger_Config::getProperties();
                $messengerConfig = Messenger_Config::getInstance();

                $messengerConfig->set(Messenger_Config::DOMAIN, isset($config['messenger']['domain']) ?
                            $config['messenger']['domain'] :
                            $properties[Messenger_Config::DOMAIN]['default']);

                $messengerConfig->set(Messenger_Config::RESOURCE, isset($config['messenger']['resource']) ?
                            $config['messenger']['resource'] :
                            $properties[Messenger_Config::RESOURCE]['default']);

                $messengerConfig->set(Messenger_Config::FORMAT, isset($config['messenger']['format']) ?
                            $config['messenger']['format'] :
                            $properties[Messenger_Config::FORMAT]['default']);

                $messengerConfig->set(Messenger_Config::SERVER_URL, isset($config['messenger']['rtmfpServerUrl']) ?
                            $config['messenger']['rtmfpServerUrl'] :
                            $properties[Messenger_Config::SERVER_URL]['default']);

                $messengerConfig->set(Messenger_Config::TEMP_FILES, isset($config['messenger']['tempFiles']) ?
                            $config['messenger']['tempFiles'] :
                            $properties[Messenger_Config::TEMP_FILES]['default']);
            }
        }

        $this->setApplicationVersion('Messenger', '1.2');
    }
}