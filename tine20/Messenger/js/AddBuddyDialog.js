Ext.ns('Tine.Messenger');

Tine.Messenger.AddBudyDialog = Ext.extend(Ext.Window,  {
   
    app: null,
    title: '',
    closeAction: 'close',
    width: 640,
    height:400,
    minWidth: 300,
    minHeight: 200,
    layout: 'fit',
    store: null,
    searchField: null,
   
    initComponent: function() {
        
        var that = this;
       
        this.app = Tine.Tinebase.appMgr.get('Messenger');

        this.title = this.app.i18n._('Add Contact');
        
        this.store = new Ext.data.Store({
            url: 'index.php',
            baseParams: {
                method: 'Messenger.searchContacts',
                name: ''
            },
            remoteSort: true,
            reader: new Ext.data.JsonReader({
                root: 'results',
                totalProperty: 'totalcount',
                id: 'id',
                fields: Tine.Messenger.Model.AddBuddy
            }),
            listeners: {
                scope: this,
                'beforeload': this.onStoreBeforeLoad
            }
        });
        
        this.searchField = new Tine.Messenger.ComboSearch({
            id: 'messenger-contact-add-name',
            fieldLabel: this.app.i18n._('Name'),
            disabled: false,
            translation: this.app.i18n,
            gridStore: this.store,
            anchor:'100%'
        });

        this.items = new Ext.FormPanel({
            id: 'messenger-contact-add-client',
            border: false,
            fieldDefaults: {
                labelWidth: 40
            },
            defaultType: 'textfield',
            bodyPadding: 5,
            buttons: [
                {
                    text: _('Add'),
                    listeners: {
                        click: function(b, e) {
                            Tine.Messenger.Window.AddBuddyHandler(that);
                        }
                    }
                },
                {   text: _('Cancel'),
                    listeners: {
                        click: function(b, e) {
                            that.close();
                        }
                    }
                }
            ],
            listeners: {
                render: function(e){
                    Ext.getCmp('messenger-contact-add-group')
                       .store
                       .loadData(
                            Tine.Messenger.RosterTree().getGroupsFromTree()
                       );
                }
            },
            items: [
                this.searchField,
                {
                    xtype: 'textfield',
                    id: 'messenger-contact-add-jid',
                    fieldLabel: this.app.i18n._('User'),
                    value: '',
                    disabled: false,
                    anchor:'100%'
                },
                {
                    xtype: 'combo',
                    id: 'messenger-contact-add-group',
                    fieldLabel: this.app.i18n._('Group'),
                    store: new Ext.data.SimpleStore({
                            id: 0,
                            fields: ['text']
                    }),
                    emptyText: this.app.i18n._('Select a group') + '...',
                    valueField: 'text',
                    displayField: 'text',
                    triggerAction: 'all',
                    editable: false,
                    mode : 'local',
                    anchor:'100%'
                },
                {
                    xtype: 'grid',
                    id: 'messenger-contact-add-grid',
                    store: this.store,
                    columns: [
                        { id: 'full_name', header: _('Name'), width: 300, sortable: true, dataIndex: 'full_name'}, 
                        { id: 'email', header: _('E-mail'), width: 300, sortable: true, dataIndex: 'email'}
                    ],
                    bbar: new Ext.PagingToolbar({
                        store: this.store,
                        displayInfo: false,
                        pageSize: 10
                    }),
                    listeners: {
                        rowclick: function(grid, rowIndex, e) {
                            var record = grid.getStore().getAt(rowIndex);
                            that.searchField.setData(record.data);
                        }
                    },
                    anchor: '100% -63'
                }
            ]
        });
        
        this.store.load({params:{name:'', start:0, limit: 10}});

        Tine.Messenger.WindowConfig.superclass.initComponent.call(this);
   },

    /**
     * called before store loads
     *
     * @param {Ext.data.Store} store
     * @param {Object} options object from the load call
     */
    onStoreBeforeLoad: function(store, options) {
        options = options || {};
        options.params = options.params || {};
        options.params.name = options.params.name ? options.params.name : null;

        var value = this.searchField.getValue();
        if (value && !options.params.name) {
            options.params.name = value;
        }
    }
   
});