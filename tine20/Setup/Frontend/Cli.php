<?php
/**
 * Tine 2.0
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schüle <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2008-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @todo        add ext check again
 */

/**
 * cli server
 *
 * This class handles all requests from cli scripts
 *
 * @package     Tinebase
 */
class Setup_Frontend_Cli
{
    /**
     * the internal name of the application
     *
     * @var string
     */
    protected $_appname = 'Setup';

    /**
     * authentication
     *
     * @param string $_username
     * @param string $_password
     * 
     * @return boolean
     */
    public function authenticate($_username, $_password)
    {
        return false;
    }
    
    /**
     * handle request (call -ApplicationName-_Cli.-MethodName- or -ApplicationName-_Cli.getHelp)
     *
     * @param Zend_Console_Getopt $_opts
     * @return void
     */
    public function handle(Zend_Console_Getopt $_opts)
    {
        Setup_Core::set(Setup_Core::USER, 'setupuser');
        
        if(isset($_opts->install)) {
            $this->_install($_opts);
        } elseif(isset($_opts->update)) {
            $this->_update($_opts);
        } elseif(isset($_opts->uninstall)) {
            $this->_uninstall($_opts);
        } elseif(isset($_opts->list)) {
            $this->_listInstalled();
        } elseif(isset($_opts->sync_accounts_from_ldap)) {
            $this->_importAccounts($_opts);
        } elseif(isset($_opts->sync_passwords_from_ldap)) {
            $this->_syncPasswords($_opts);
        } elseif(isset($_opts->egw14import)) {
            $this->_egw14Import($_opts);
        } elseif(isset($_opts->check_requirements)) {
            $this->_checkRequirements($_opts);
        } elseif(isset($_opts->setconfig)) {
            $this->_setConfig($_opts);
        } elseif(isset($_opts->create_admin)) {
            $this->_createAdminUser($_opts);
        } elseif(isset($_opts->toggle_multidomain)) {
            $this->_toggleMultidomain();
        } elseif(isset($_opts->multidomain_status)) {
            $this->_getMultidomainStatus();
        } elseif(isset($_opts->set_default_domain)) {
            $this->_setDefaultDomain($_opts);
        } elseif(isset($_opts->create_new_domain)) {
            $this->_createNewDomain($_opts);
        } elseif(isset($_opts->apply_customizations)){
            Setup_Controller::getInstance()->applyCustomizations();
        }
    }
    
    /**
     * install new applications
     *
     * @param Zend_Console_Getopt $_opts
     */
    protected function _install(Zend_Console_Getopt $_opts)
    {
        $controller = Setup_Controller::getInstance();

        if($_opts->install === true) {
            $applications = $controller->getInstallableApplications();
            $applications = array_keys($applications);
        } else {
            $applications = array();
            $applicationNames = explode(',', $_opts->install);
            foreach($applicationNames as $applicationName) {
                $applicationName = ucfirst(trim($applicationName));
                try {
                    $controller->getSetupXml($applicationName);
                    $applications[] = $applicationName;
                } catch (Setup_Exception_NotFound $e) {
                    echo "Application $applicationName not found! Skipped...\n";
                }
            }
        }

        $options = $this->_parseRemainingArgs($_opts->getRemainingArgs());
        $this->_promptRemainingOptions($applications, $options);

        $controller->installApplications($applications, $options);
        
        if (array_key_exists('acceptedTermsVersion', $options)) {
            Setup_Controller::getInstance()->saveAcceptedTerms($options['acceptedTermsVersion']);
        }
        
        echo "Successfully installed " . count($applications) . " applications.\n";
    }

    /**
     * prompt remaining options
     * 
     * @param array $_applications
     * @param array $_options
     * @return void
     * 
     * @todo add required version server side
     */
    protected function _promptRemainingOptions($_applications, &$_options) {
        if (in_array('Tinebase', $_applications)) {
            
            if (! isset($_options['acceptedTermsVersion'])) {
                fwrite(STDOUT, PHP_EOL . file_get_contents(dirname(dirname(dirname(__FILE__))) . '/LICENSE' ));
                $licenseAnswer = Tinebase_Server_Cli::promptInput('I have read the license agreement and accept it (type "yes" to accept)');
                
                
                fwrite(STDOUT, PHP_EOL . file_get_contents(dirname(dirname(dirname(__FILE__))) . '/PRIVACY' ));
                $privacyAnswer = Tinebase_Server_Cli::promptInput('I have read the privacy agreement and accept it (type "yes" to accept)');
            
                if (! (strtoupper($licenseAnswer) == 'YES' && strtoupper($privacyAnswer) == 'YES')) {
                    echo "error: you need to accept the terms! exiting \n";
                    exit (1);
                }
                
                $_options['acceptedTermsVersion'] = 1;
            }
            
            
            // initial username
            if (! isset($_options['adminLoginName'])) {
                $_options['adminLoginName'] = Tinebase_Server_Cli::promptInput('Inital Admin Users Username');
                if (! $_options['adminLoginName']) {
                    echo "error: username must be given! exiting \n";
                    exit (1);
                }
            }
            
            // initial password
            if (! isset($_options['adminPassword'])) {
                $_options['adminPassword'] = $this->_promptPassword();
            }
        }
    }
    
    /**
     * prompt password
     * 
     * @return string
     */
    protected function _promptPassword()
    {
        $password1 = Tinebase_Server_Cli::promptInput('Admin user password', TRUE);
        if (! $password1) {
            echo "Error: Password must not be empty! Exiting ... \n";
            exit (1);
        }
        $password2 = Tinebase_Server_Cli::promptInput('Confirm password', TRUE);
        if ($password1 !== $password2) {
            echo "Error: Passwords do not match! Exiting ... \n";
            exit (1);
        }
        
        return $password1;
    }
    
    /**
     * update existing applications
     *
     * @param Zend_Console_Getopt $_opts
     */
    protected function _update(Zend_Console_Getopt $_opts)
    {
        $controller = Setup_Controller::getInstance();
        
        if ($_opts->update === true) {
            $applications = Tinebase_Application::getInstance()->getApplications(NULL, 'id');
        } else {
            $applications = new Tinebase_Record_RecordSet('Tinebase_Model_Application');
            $applicationNames = explode(',', $_opts->update);
            foreach($applicationNames as $applicationName) {
                $applicationName = ucfirst(trim($applicationName));
                try {
                    $application = Tinebase_Application::getInstance()->getApplicationByName($applicationName);
                    $applications->addRecord($application);
                } catch (Tinebase_Exception_NotFound $e) {
                    //echo "Application $applicationName is not installed! Skipped...\n";
                }
            }
        }
        
        foreach ($applications as $key => &$application) {
            try {
                if (!$controller->updateNeeded($application)) {
                    //echo "Application $application is already up to date! Skipped...\n";
                    unset($applications[$key]);
                }
            } catch (Setup_Exception_NotFound $e) {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Failed to check if an application needs an update:' . $e->getMessage());
                unset($applications[$key]);
            }
        }

        $updatecount = 0;
        if (count($applications) > 0) {
            $result = $controller->updateApplications($applications);
            $updatecount = $result['updated'];
        }
        
        echo "Updated " . $updatecount . " applications.\n";
    }

    /**
     * uninstall applications
     *
     * @param Zend_Console_Getopt $_opts
     */
    protected function _uninstall(Zend_Console_Getopt $_opts)
    {
        $controller = Setup_Controller::getInstance();
        
        if($_opts->uninstall === true) {
            try {
                $applications = Tinebase_Application::getInstance()->getApplications(NULL, 'id');
            } catch(Zend_Db_Statement_Exception $e) {
                echo "No applications installed\n";
                return;
            }
        } else {
            $applications = new Tinebase_Record_RecordSet('Tinebase_Model_Application');
            $applicationNames = explode(',', $_opts->uninstall);
            foreach($applicationNames as $applicationName) {
                $applicationName = ucfirst(trim($applicationName));
                try {
                    $application = Tinebase_Application::getInstance()->getApplicationByName($applicationName);
                    $applications->addRecord($application);
                } catch (Tinebase_Exception_NotFound $e) {
                    echo "Application $applicationName is not installed! Skipped...\n";
                }
            }
        }
        
        $controller->uninstallApplications($applications->name);

        echo "Successfully uninstalled " . count($applications) . " applications.\n";
    }

    /**
     * list installed apps
     *
     */
    protected function _listInstalled()
    {
        try {
            $applications = Tinebase_Application::getInstance()->getApplications(NULL, 'id');
        } catch (Zend_Db_Statement_Exception $e) {
            echo "No applications installed\n";
            return;
        }
        
        echo "Currently installed applications:\n";
        foreach($applications as $application) {
            echo "* $application\n";
        }
    }
    
    /**
     * import accounts from ldap
     *
     * @param Zend_Console_Getopt $_opts
     */
    protected function _importAccounts(Zend_Console_Getopt $_opts)
    {
        $options = $this->_parseRemainingArgs($_opts->getRemainingArgs());

        $locale = 'en';
        if (!empty($options['locale'])) {
            $locale = $options['locale'];
        }

        Tinebase_Core::setupUserLocale($locale);
        Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Set locale to: ' . $locale);

        // disable timelimit during import of user accounts
        Setup_Core::setExecutionLifeTime(0);
        
        // import groups
        Tinebase_Group::syncGroups();
        
        // import users
        $options = array('syncContactData' => TRUE);
        if ($_opts->dbmailldap) {
            $options['ldapplugins'] = array(
                new Tinebase_EmailUser_Imap_LdapDbmailSchema(),
                new Tinebase_EmailUser_Smtp_LdapDbmailSchema()
            );
        }
        Tinebase_User::syncUsers($options);
    }
    
    /**
     * sync ldap passwords
     * 
     * @param Zend_Console_Getopt $_opts
     */
    protected function _syncPasswords(Zend_Console_Getopt $_opts)
    {
        Tinebase_User::syncLdapPasswords();
    }
    
    /**
     * import from egw14
     * 
     * @param Zend_Console_Getopt $_opts
     */
    protected function _egw14Import(Zend_Console_Getopt $_opts)
    {
        $args = $_opts->getRemainingArgs();
        
        if (count($args) < 1 || ! is_readable($args[0])) {
            echo "can not open config file \n";
            echo "see tine20.org/wiki/EGW_Migration_Howto for details \n\n";
            echo "usage: ./setup.php --egw14import /path/to/config.ini (see Tinebase/Setup/Import/Egw14/config.ini)\n\n";
            exit(1);
        }
        
        try {
            $config = new Zend_Config(array(), TRUE);
            $config->merge(new Zend_Config_Ini($args[0]));
            $config = $config->merge($config->all);
        } catch (Zend_Config_Exception $e) {
            fwrite(STDERR, "Error while parsing config file($args[0]) " .  $e->getMessage() . PHP_EOL);
            exit(1);
        }
        
        $writer = new Zend_Log_Writer_Stream('php://output');
        $logger = new Zend_Log($writer);
        
        $filter = new Zend_Log_Filter_Priority((int) $config->loglevel);
        $logger->addFilter($filter);
        
        $importer = new Tinebase_Setup_Import_Egw14($config, $logger);
        $importer->import();
    }
    
    /**
     * do the environment check
     *
     * @return array
     */
    protected function _checkRequirements(Zend_Console_Getopt $_opts)
    {
        $results = Setup_Controller::getInstance()->checkRequirements();
        if ($results['success']) {
          echo "OK - All requirements are met\n";
        } else {
          echo "ERRORS - The following requirements are not met: \n";
          foreach ($results['results'] as $result) {
            if (!empty($result['message'])) {
              echo "- " . strip_tags($result['message']) . "\n";
            }
          }
        }
    }
    
    /**
     * set config
     *
     * @return array
     */
    protected function _setConfig(Zend_Console_Getopt $_opts)
    {
        $options = $this->_parseRemainingArgs($_opts->getRemainingArgs());
        $errors = array();
        if (empty($options['configkey'])) {
            $errors[] = 'Missing argument: configkey';
        }
        if (! isset($options['configvalue'])) {
            $errors[] = 'Missing argument: configvalue';
        }
        $configKey = (string)$options['configkey'];
        $configValue = self::parseConfigValue($options['configvalue']);
        
        $applicationName = (isset($options['app'])) ? $options['app'] : 'Tinebase';
        
        if (empty($errors)) {
           Setup_Controller::getInstance()->setConfigOption($configKey, $configValue, $applicationName);
           echo "OK - Updated configuration option $configKey for application $applicationName\n";
        } else {
            echo "ERRORS - The following errors occured: \n";
            foreach ($errors as $error) {
                echo "- " . $error . "\n";
            }
        }
    }
    
    /**
     * create admin user / activate existing user / allow to reset password
     * 
     * @param Zend_Console_Getopt $_opts
     * 
     * @todo check role by rights and not by name
     */
    protected function _createAdminUser(Zend_Console_Getopt $_opts)
    {
        if (! Setup_Controller::getInstance()->isInstalled('Tinebase')) {
            die('Install Tinebase first.');
        }
        
        echo "Please enter a username. If the user already exists, he is reactivated and you can reset the password.\n";
        $username = Tinebase_Server_Cli::promptInput('Username');
        $tomorrow = Tinebase_DateTime::now()->addDay(1);
        
        try {
            $user = Tinebase_User::getInstance()->getFullUserByLoginName($username);
            echo "User $username already exists.\n";
            Tinebase_User::getInstance()->setStatus($user->getId(), Tinebase_Model_User::ACCOUNT_STATUS_ENABLED);
            echo "Activated admin user '$username'.\n";
            
            $expire = Tinebase_Server_Cli::promptInput('Should the admin user expire tomorrow (default: "no", "y" or "yes" for expiry)?');
            if ($expire === 'y' or $expire === 'yes') {
                Tinebase_User::getInstance()->setExpiryDate($user->getId(), $tomorrow);
                echo "User expires tomorrow at $tomorrow.\n";
            }
            
            $resetPw = Tinebase_Server_Cli::promptInput('Do you want to reset the password (default: "no", "y" or "yes" for reset)?');
            if ($resetPw === 'y' or $resetPw === 'yes') {
                $password = $this->_promptPassword();
                Tinebase_User::getInstance()->setPassword($user, $password);
                echo "User password has been reset.\n";
            }
            
            // check admin group membership
            $adminGroup = Tinebase_Group::getInstance()->getDefaultAdminGroup();
            $memberships = Tinebase_Group::getInstance()->getGroupMemberships($user);
            if (! in_array($adminGroup->getId(), $memberships)) {
                try {
                    Tinebase_Group::getInstance()->addGroupMember($adminGroup, $user);
                    echo "Added user to default admin group\n";
                } catch (Zend_Ldap_Exception $zle) {
                    echo "Could not add user to default admin group: " . $zle->getMessage();
                }
            }
            
            $this->_checkAdminRole($user);
            
        } catch (Tinebase_Exception_NotFound $tenf) {
            if (Tinebase_User::getConfiguredBackend() === Tinebase_User::LDAP) {
                die('It is not possible to create a new user with LDAP user backend here.');
            }
            
            // create new admin user that expires tomorrow
            $password = $this->_promptPassword();
            Tinebase_User::createInitialAccounts(array(
                'adminLoginName' => $username,
                'adminPassword'  => $password,
                'expires'        => $tomorrow,
            ));
            echo "Created new admin user '$username' that expires tomorrow.\n";
        }
    }
    
    /**
     * check admin role membership
     * 
     * @param Tinebase_Model_FullUser $user
     */
    protected function _checkAdminRole($user)
    {
        $roleMemberships = Tinebase_Acl_Roles::getInstance()->getRoleMemberships($user->getId());
        $adminRoleFound = FALSE;
        foreach ($roleMemberships as $roleId) {
            $role = Tinebase_Acl_Roles::getInstance()->getRoleById($roleId);
            if ($role->name === 'admin role') {
                $adminRoleFound = TRUE;
                break;
            }
        }
        
        if (! $adminRoleFound) {
            echo "Admin role not found for user " . $user->accountLoginName . ".\n";
            $adminRole = new Tinebase_Model_Role(array(
                'name'                  => 'admin role',
                'description'           => 'admin role for tine. this role has all rights per default.',
            ));
            $adminRole = Tinebase_Acl_Roles::getInstance()->createRole($adminRole);
            Tinebase_Acl_Roles::getInstance()->setRoleMembers($adminRole->getId(), array(
                array(
                    'id'    => $user->getId(),
                    'type'  => Tinebase_Acl_Rights::ACCOUNT_TYPE_USER, 
                )
            ));
            
            // add all rights for all apps
            $enabledApps = Tinebase_Application::getInstance()->getApplicationsByState(Tinebase_Application::ENABLED);
            $roleRights = array();
            foreach ($enabledApps as $application) {
                $allRights = Tinebase_Application::getInstance()->getAllRights($application->getId());
                foreach ($allRights as $right) {
                    $roleRights[] = array(
                        'application_id' => $application->getId(),
                        'right'          => $right,
                    );
                }
            }
            Tinebase_Acl_Roles::getInstance()->setRoleRights($adminRole->getId(), $roleRights);
            
            echo "Created admin role for user " . $user->accountLoginName . ".\n";
        }
    }
    
    /**
     * parse options
     * 
     * @param string $_value
     * @return array|string
     */
   public static function parseConfigValue($_value)
    {
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($_value, TRUE));

        // check value is json encoded
        if (Tinebase_Helper::is_json($_value)) {
            return Zend_Json::decode($_value);
        }

        $result = array(
            'active' => 1
        );

        // keep spaces, \: and \,
        $_value = preg_replace(array('/ /', '/\\\:/', '/\\\,/', '/\s*/'), array('§', '@', ';', ''), $_value);

        $parts = explode(',', $_value);

        foreach ($parts as $part) {
            $part = str_replace(';', ',', $part);
            $part = str_replace('§', ' ', $part);
            $part = str_replace('@', ':', $part);
            if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . $part);
            if (strpos($part, '_') !== FALSE) {
                list($key, $sub) = preg_split('/_/', $part, 2);
                if (preg_match('/:/', $sub)) {
                    list($subKey, $value) = explode(':', $sub);
                    $result[$key][$subKey] = $value;
                } else {
                    // might be a '_' in the value
                    if (preg_match('/:/', $part)) {
                        $exploded = explode(':', $part);
                        $key = array_shift($exploded);
                        $result[$key] = implode(':', $exploded);
                    } else {
                        throw new Tinebase_Exception_UnexpectedValue('You have an error in the config syntax (":" expected): ' . $part);
                    }
                }
            } else {
                if (strpos($part, ':') !== FALSE) {
                    list($key, $value) = preg_split('/:/', $part, 2);
                    $result[$key] = $value;
                } else {
                    $result = $part;
                }
            }
        }

        return $result;
    }
    
    /**
     * parse remaining args
     * 
     * @param string $_args
     * @return array
     */
    protected function _parseRemainingArgs($_args)
    {
        $options = array();
        foreach ($_args as $arg) {
            if (strpos($arg, '=') !== FALSE) {
                if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . $arg);
                list($key, $value) = preg_split('/=/', $arg, 2);
                $options[$key] = $value;
            }
        }

        return $options;
    }

    /**
     * Enables or disables multidomain in init_plugins.php file
     *
     * @return void
     */
    protected function _toggleMultidomain()
    {
        $initPluginsSource = file_get_contents("init_plugins.php");
        if(!$this->_isThereMultidomainConfiguration($initPluginsSource)) {
            $initPluginsSource .= "\n// Multidomain configuration\nTinebase_Config_Manager::setMultidomain(TRUE);";
        } else {
            if($this->_isMultidomainEnabled($initPluginsSource)) {
                $initPluginsSource = preg_replace("/Tinebase_Config_Manager::setMultidomain\(TRUE\)/i",
                        "Tinebase_Config_Manager::setMultidomain(FALSE)", $initPluginsSource);
            } else {
                $initPluginsSource = preg_replace("/Tinebase_Config_Manager::setMultidomain\(FALSE\)/i",
                        "Tinebase_Config_Manager::setMultidomain(TRUE)", $initPluginsSource);
            }
        }

        file_put_contents("init_plugins.php", $initPluginsSource);
        $this->_getMultidomainStatus();
    }

    /**
     * Checks if is there multidomain configuration in source code provided
     *
     * @param string $initPluginsSource
     * @return boolean
     */
    protected function _isThereMultidomainConfiguration($initPluginsSource)
    {
        return preg_match("/Tinebase_Config_Manager::setMultidomain\((TRUE|FALSE)\)/i", $initPluginsSource);
    }

    /**
     * Checks if multidomain is enabled or not in source code provided
     *
     * @param string $initPluginsSource
     * @return boolean
     */
    protected function _isMultidomainEnabled($initPluginsSource)
    {
        if(!$this->_isThereMultidomainConfiguration($initPluginsSource)) {
            return false;
        }

        return preg_match("/Tinebase_Config_Manager::setMultidomain\(TRUE\)/i", $initPluginsSource);
    }

    /**
     * Gets multidomain status in init_plugins.php source
     *
     * @return void
     */
    protected function _getMultidomainStatus()
    {
        $initPluginsSource = file_get_contents("init_plugins.php");
        if($this->_isMultidomainEnabled($initPluginsSource)) {
            $domaindata = Tinebase_Config::getInstance()->get('domaindata');
            echo "Multidomain is enabled\n";
            $domainContent = scandir("domains");
            foreach($domainContent as $content) {
                if (is_dir("domains/$content")) {
                    if($content == "." || $content == "..") {
                        continue;
                    }

                    if($content == $domaindata['domain']) {
                        echo $domaindata['domain']." (default) \n";
                    } else {
                        echo "$content\n";
                    }
                }
            }
        } else {
            echo "Multidomain is disabled\n";
        }
    }

    /**
     * Set the default domain to install
     *
     * @param object $_opts
     */
    protected function _setDefaultDomain($_opts)
    {
        $domain = $_opts->set_default_domain;
        $initPluginsSource = file_get_contents("init_plugins.php");
        if(!$this->_isMultidomainEnabled($initPluginsSource)) {
            echo "Multidomain is disabled. Enable it first!\n";
        } else {
            try {
                Tinebase_Config::setDomain($domain);
                Tinebase_Config::getInstance()->set('domaindata', array('domain' => $domain));
                echo "Default domain set to \"$domain\"\n";
            } catch (Exception $ex) {
                echo "Impossible to set domain to \"$domain\": ".$ex->getMessage()."\n";
            }
        }
    }

    /**
     * Creates the directory for the new domain, copies the content, permissions and ownerships
     * from the other domain provided
     *
     * @param array $_opts
     * @return void
     */
    protected function _createNewDomain($_opts)
    {
        $newDomain = $_opts->create_new_domain;
        $remainingArgs = $this->_parseRemainingArgs($_opts->getRemainingArgs());
        $authenticationData = isset($remainingArgs['authenticationData']) ? $remainingArgs['authenticationData'] : null;

        $authenticationBackendType = Tinebase_Auth::SQL;
        if(isset($authenticationData['authentication'])) {
            if(isset($authenticationData['authentication']['backend'])) {
                $authenticationBackendType = $authenticationData['authentication']['backend'];
            }
        }

        $usersBackendType = Tinebase_User::SQL;
        if(isset($authenticationData['accounts'])) {
            if(isset($authenticationData['accounts']['backend'])) {
                $usersBackendType = $authenticationData['accounts']['backend'];
            }
        }

        $configTemplateArray = $this->_getAuthenticationAccountsTemplate($authenticationBackendType, $usersBackendType);

        $emailDataArray = array();
        if(isset($remainingArgs['imap'])) {
            if(isset($remainingArgs['imap']['backend'])) {
                $emailDataArray['imap'] = $this->_getEmailImapTemplate($remainingArgs['imap']['backend']);
            } else {
                $emailDataArray['imap'] = $this->_getEmailImapTemplate();
            }
        }

        if(isset($remainingArgs['smtp'])) {
            if(isset($remainingArgs['smtp']['backend'])) {
                $emailDataArray['smtp'] = $this->_getEmailSmtpTemplate($remainingArgs['smtp']['backend']);
            } else {
                $emailDataArray['smtp'] = $this->_getEmailSmtpTemplate();
            }
        }

        $emailDataArray['sieve'] = $this->_getEmailSieveTemplate();

        $databaseTemplateArray = $this->_getDatabaseTemplate();

        $configTemplateArray = array_merge($configTemplateArray, $emailDataArray);
        $configTemplateArray = array_merge($configTemplateArray, $databaseTemplateArray);

        $configTemplate = new Zend_Config($configTemplateArray);

        $newDomainPath   = "domains/$newDomain";
        if(file_exists($newDomainPath)) {
            echo "The new domain \"$newDomain\" already exists.\n";
            return;
        } else {
            mkdir($newDomainPath);
        }

        $configFile = "$newDomainPath/config.inc.php";

        $writer = new Zend_Config_Writer_Array(array(
            'config'   => $configTemplate,
            'filename' => $configFile,
        ));

        $writer->write();

        echo "New domain \"$newDomain\" created. Set the new domain as default before continue installation.\n";
    }

    /**
     * Returns authentication and accounts config template
     *
     * @param string $authenticationBackendType
     * @param string $accountsBackendType
     * @return array
     */
    protected function _getAuthenticationAccountsTemplate($authenticationBackendType = Tinebase_Auth::SQL,
            $accountsBackendType = Tinebase_User::SQL) {

        $result[Tinebase_Config::AUTHENTICATIONBACKENDTYPE] = $authenticationBackendType;
        $result[Tinebase_Config::AUTHENTICATIONBACKEND] = $this->_getAuthenticationTemplate($authenticationBackendType);

        $result[Tinebase_Config::USERBACKENDTYPE] = $accountsBackendType;
        $result[Tinebase_Config::USERBACKEND] = $this->_getAccountsTemplate($accountsBackendType);

        $result['saveusername']['saveusername'] = false;

        $result['password']['changepw'] = true;
        $result['password']['pwPolicyActive'] = false;
        $result['password']['pwPolicyOnlyASCII'] = false;
        $result['password']['pwPolicyMinLength'] = 0;
        $result['password']['pwPolicyMinWordChars'] = 0;
        $result['password']['pwPolicyMinUppercaseChars'] = 0;
        $result['password']['pwPolicyMinSpecialChars'] = 0;
        $result['password']['pwPolicyMinNumbers'] = 0;
        $result['password']['pwPolicyForbidUsername'] = false;
        $result['password']['pwPolicyForbidSamePassword'] = false;

        $result['redirectSettings']['redirectUrl'] = "";
        $result['redirectSettings']['redirectAlways'] = false;
        $result['redirectSettings']['redirectToReferrer'] = false;

        return $result;
    }

    /**
     * Returns accounts config template
     *
     * @param string $backend
     * @return array
     * @throws Tinebase_Exception_InvalidArgument
     * @throws Tinebase_Exception
     */
    protected function _getAccountsTemplate($backend = Tinebase_User::SQL)
    {
        $pattern = sprintf('/(%s|%s)/', Tinebase_User::SQL, Tinebase_User::LDAP);
        if(!preg_match($pattern, ucfirst($backend))) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Accounts backend type is not valid: $backend");
            throw new Tinebase_Exception_InvalidArgument("Accounts backend type is not valid: $backend");
        }

        $backendConfig = array();

        switch($backend) {
            case Tinebase_User::LDAP:
                $backendConfig['host'] = "";
                $backendConfig['username'] = "";
                $backendConfig['password'] = "";
                $backendConfig['bindRequiresDn'] = true;
                $backendConfig['syncWhenNotFound'] = true;
                $backendConfig['userDn'] = "";
                $backendConfig['userOus'] = "";
                $backendConfig['userLoginForUserIdgeneration'] = false;
                $backendConfig['userFilter'] = "objectclass=posixaccount";
                $backendConfig['extraSchemas'] = "";
                $backendConfig['extraAttributes'] = "";
                $backendConfig['userSearchScope'] = Tinebase_Ldap::SEARCH_SCOPE_SUB;
                $backendConfig['groupsDn'] = "";
                $backendConfig['groupFilter'] = "objectclass=posixgroup";
                $backendConfig['groupSchemas'] = "";
                $backendConfig['groupAttributes'] = "";
                $backendConfig['groupOU'] = "";
                $backendConfig['groupSearchScope'] = Tinebase_Ldap::SEARCH_SCOPE_SUB;
                $backendConfig['pwEncType'] = "PLAIN";
                $backendConfig['useRfc2307bis'] = false;
                $backendConfig['minUserId'] = 10000;
                $backendConfig['maxUserId'] = 29999;
                $backendConfig['minGroupId'] = 11000;
                $backendConfig['maxGroupId'] = 11099;
                $backendConfig['groupUUIDAttribute'] = "entryUUID";
                $backendConfig['userUUIDAttribute'] = "entryUUID";
                $backendConfig['saveStatus'] = false;
                $backendConfig['readonly'] = true;
                $backendConfig['masterLdapHost'] = "";
                $backendConfig['masterLdapUsername'] = "";
                $backendConfig['masterLdapPassword'] = "";
                $backendConfig['displaynameFormat'] = "";
                $backendConfig['displayName'] = "";
                $backendConfig['fullName'] = "";
                $backendConfig['checkExpiredPassword'] = false;
                $backendConfig['mailListControl'] = false;
                $backendConfig['passwordExpirationAttribute'] = "";
                $backendConfig['passwordExpirationInterval'] = "";
                $backendConfig['mailListDn'] = "";
                $backendConfig['mailListGid'] = "";
                $backendConfig['mailListFilter'] = "";
                $backendConfig['mailListSchemas'] = "";
                $backendConfig['mailListAttributes'] = "";
            case Tinebase_User::SQL:
                $backendConfig['defaultAdminGroupName'] = "Users";
                $backendConfig['defaultUserGroupName']  = "Administrators";
                break;
            default:
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Wrong backend type for accounts: $backend");
                throw new Tinebase_Exception("Wrong backend type for accounts: $backend");
        }

        return $backendConfig;
    }

    /**
     * Returns authentication config template
     *
     * @param string $backend
     * @return string
     * @throws Tinebase_Exception_InvalidArgument
     */
    protected function _getAuthenticationTemplate($backend = Tinebase_Auth::SQL)
    {
        $pattern = sprintf('/(%s|%s|%s)/', Tinebase_Auth::SQL, Tinebase_Auth::LDAP, Tinebase_Auth::IMAP);
        if(!preg_match($pattern, ucfirst($backend))) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Authentication backend type is not valid: $backend");
            throw new Tinebase_Exception_InvalidArgument("Authentication backend type is not valid: $backend");
        }

        $backendConfig = array();
        switch($backend) {
            case Tinebase_Auth::SQL:
                $backendConfig['tryUsernameSplit'] = true;
                $backendConfig['accountCanonicalForm'] = "";
                $backendConfig['accountDomainName'] = "";
                $backendConfig['accountDomainNameShort'] = "";
                break;
            case Tinebase_Auth::LDAP:
                $backendConfig['host'] = "";
                $backendConfig['username'] = "";
                $backendConfig['password'] = "";
                $backendConfig['bindRequiresDn'] = "";
                $backendConfig['baseDn'] = "";
                $backendConfig['accountFilterFormat'] = "";
                $backendConfig['tryUsernameSplit'] = "";
                $backendConfig['accountCanonicalForm'] = "";
                $backendConfig['accountDomainName'] = "";
                $backendConfig['accountDomainNameShort'] = "";
                break;
            case Tinebase_Auth::IMAP:
                $backendConfig['host'] = "";
                $backendConfig['port'] = "";
                $backendConfig['ssl'] = "none";
                $backendConfig['domain'] = "";
                break;
            default:
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Wrong backend type for authentication: $backend");
                throw new Tinebase_Exception_InvalidArgument("Wrong backend type for authentication: $backend");
        }

        return $backendConfig;
    }

    /**
     * Returns Imap config template
     *
     * @param string $backend
     * @return array
     * @throws Tinebase_Exception_InvalidArgument
     */
    protected function _getEmailImapTemplate($backend = 'standard')
    {
        $result = array();

        switch($backend) {
            case 'standard';
                break;
            case Tinebase_EmailUser::CYRUS:
                $result[$backend]['admin'] = "";
                $result[$backend]['password'] = "";
                $result[$backend]['useProxyAuth'] = false;
                break;
            case Tinebase_EmailUser::DOVECOT_IMAP:
                $result[$backend]['uid'] = "";
                $result[$backend]['gid'] = "";
                $result[$backend]['home'] = "";
                $result[$backend]['scheme'] = "PLAIN-MD5";
            case Tinebase_EmailUser::DBMAIL:
                $result[$backend]['host'] = "";
                $result[$backend]['dbname'] = "";
                $result[$backend]['username'] = "";
                $result[$backend]['password'] = "";
                $result[$backend]['port'] = 3306;
                break;
            case Tinebase_EmailUser::LDAP_IMAP:
                break;
            default:
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Wrong backend type for IMAP: $backend");
                throw new Tinebase_Exception_InvalidArgument("Wrong backend type for IMAP: $backend");
        }

        $result['active'] = false;
        $result['backend'] = $backend;
        $result['host'] = "";
        $result['port'] = 0;
        $result['quota_default'] = 0;
        $result['quota_total'] = 0;
        $result['ssl'] = "";
        $result['useSystemAccount'] = false;
        $result['useEmailAsLoginName'] = false;
        $result['domain'] = "";

        return $result;
    }

    /**
     * Return smtp config template
     *
     * @param string $backend
     * @return array
     * @throws Tinebase_Exception_InvalidArgument
     */
    protected function _getEmailSmtpTemplate($backend = 'standard')
    {
        $result = array();

        switch($backend) {
            case 'standard':
                break;
            case Tinebase_EmailUser::POSTFIX:
            case Tinebase_EmailUser::LDAP_SMTP_MAILALTERNATEADDRESS:
            case Tinebase_EmailUser::LDAP_SMTP_MAIL:
            case Tinebase_EmailUser::LDAP_SMTP:
            case Tinebase_EmailUser::LDAP_SMTP_QMAIL:
                $result[Tinebase_EmailUser::POSTFIX]['host'] = "";
                $result[Tinebase_EmailUser::POSTFIX]['dbname'] = "";
                $result[Tinebase_EmailUser::POSTFIX]['username'] = "";
                $result[Tinebase_EmailUser::POSTFIX]['password'] = "";
                $result[Tinebase_EmailUser::POSTFIX]['port'] = 3306;
                break;
            default:
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Wrong backend type for SMTP: $backend");
                throw new Tinebase_Exception_InvalidArgument("Wrong backend type for SMTP: $backend");
        }

        $result['backend'] = $backend;
        $result['hostname'] = "";
        $result['port'] = "";
        $result['ssl'] = "none";
        $result['auth'] = "none";
        $result['primarydomain'] = "";
        $result['secondarydomains'] = "";
        $result['obligatorydomains'] = "";
        $result['from'] = "";
        $result['username'] = "";
        $result['password'] = "";
        $result['name'] = "localhost";

        return $result;
    }

    /**
     * Returns sieve config template
     *
     * @return array
     */
    protected function _getEmailSieveTemplate()
    {
        $result = array();

        $result['hostname'] = "";
        $result['port'] = "";
        $result['ssl'] = "none";
        $result['proxy_user'] = "";
        $result['proxy_password'] = "";
        $result['proxy_useAuth'] = false;
        $result['limitDomains'] = false;
        $result['allowedDomains'] = "";

        return $result;
    }

    /**
     * Returns global config template
     *
     * @return array
     * @TODO include a call for this method
     */
    protected function _getGlobalConfigTemplate()
    {
        $result = array();

        $result['setupuser']['username'] = "";
        $result['setupuser']['password'] = "";

        $result['domaindata']['domain'] = "";

        $result['logger']['active'] = false;
        $result['logger']['filename'] = "";
        $result['logger']['priority'] = "";

        $result['caching']['backend'] = "";
        $result['caching']['active'] = false;
        $result['caching']['lifetime'] = "";
        $result['caching']['path'] = "";
        $result['caching']['redis_host'] = "";
        $result['caching']['redis_port'] = "";
        $result['caching']['memcached_host'] = "";
        $result['caching']['memcached_port'] = "";
        $result['caching']['customexpirable'] = false;

        $result['session']['backend'] = "";
        $result['session']['lifetime'] = "";
        $result['session']['path'] = "";
        $result['session']['host'] = "";
        $result['session']['port'] = "";
        $result['session']['storeAclIntoSession'] = false;
        $result['session']['storePreferenceIntoSession'] = false;

        $result['helpdoc']['active'] = false;
        $result['helpdoc']['text'] = "";
        $result['helpdoc']['title'] = "";
        $result['helpdoc']['url'] = "";

        $result['theme']['active'] = false;
        $result['theme']['load'] = "";
        $result['theme']['path'] = "";
        $result['theme']['useBlueAsBase'] = "";
        $result['theme']['backgroundImageUrl'] = "";
        $result['theme']['messageImageUrl'] = "";
        $result['theme']['messageLinkUrl'] = "";
        $result['theme']['accessibleLinkUrl'] = "";

        $result['themes']['default'] = 0;
        $result['themes']['cookieTheme'] = "";
        $result['themes']['themelist'][0]['name'] = "Tine 2.0 Default skin";
        $result['themes']['themelist'][0]['path'] = "tine20";
        $result['themes']['themelist'][0]['useBlueAsBase'] = 1;

        $result['bugreportUrl'] = "";

        $result['sessionIpValidation']['active'] = false;
        $result['sessionIpValidation']['source'] = "";
        $result['sessionIpValidation']['header'] = "";

        return $result;
    }

    /**
     * Returns database config template
     *
     * @return array
     */
    protected function _getDatabaseTemplate()
    {
        $result = array();

        $result['database']['host'] = "";
        $result['database']['dbname'] = "";
        $result['database']['username'] = "";
        $result['database']['password'] = "";
        $result['database']['adapter'] = "pdo_mysql";
        $result['database']['tableprefix'] = "tine20_";
        $result['database']['port'] = 0;
        $result['database']['profiler'] = true;

        return $result;
    }

    /**
     * Returns config manager config template
     *
     * @return array
     * @TODO include a call for this method
     */
    protected function _getConfigManagerTemplate()
    {
        $result = array();

        $result['mappanel'] = "";

        $result['asdatabase']['active'] = false;
        $result['asdatabase']['adapter'] = 'pdo_mysql';
        $result['asdatabase']['host'] = "";
        $result['asdatabase']['port'] = "";
        $result['asdatabase']['dbname'] = "";
        $result['asdatabase']['username'] = "";
        $result['asdatabase']['password'] = "";
        $result['asdatabase']['tableprefix'] = "";

        $result['actionqueue']['active'] = false;
        $result['actionqueue']['backend'] = 'Redis';
        $result['actionqueue']['host'] = 'localhost';
        $result['actionqueue']['port'] = '6379';

        $result['tmpdir'] = "";

        $result['filesdir'] = "";

        $result['email']['maxMessageSize'] = 0;
        $result['email']['maxContactAddToUnknown'] = 0;

        $result['redirecting']['active'] = false;
        $result['redirecting']['ldapAttribute'] = "";
        $result['redirecting']['cookieName'] = "";
        $result['redirecting']['defaultCookieValue'] = "";

        $result['stateprovider']['provider'] = "";

        $result['certificate']['active'] = false;
        $result['certificate']['useKeyEscrow'] = false;
        $result['certificate']['masterCertificate'] = "";

        $result['accesslog']['disableactivesyncacceesslog'] = true;
        $result['accesslog']['disabledavaccesslog'] = true;

        return $result;
    }
}
