/*
 * Tine 2.0
 *
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/*global Ext, Tine*/

Ext.ns('Tine', 'Tine.Setup');

/**
 * Setup Authentication Manager
 *
 * @namespace   Tine.Setup
 * @class       Tine.Setup.AuthenticationPanel
 * @extends     Tine.Tinebase.widgets.form.ConfigPanel
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Setup.Custom.AuthenticationPanel
 *
 * To create a new custom authentication combo items, create a new js script and
 * adds it using the js addition system.
 *
 * The new class must extends this class below and it must have the getCustomItems
 * overriten, like in its comments.
 */
Tine.Setup.AuthenticationPanel.Custom = Ext.extend(Object, {

    /**
     * app object
     */
    app: null,

    /**
     * authProviderIdPrefix
     */
    authProviderIdPrefix: null,

    /**
     * AuthenticationPanel
     */
    authenticationPanel: null,

    /**
     * The Constructor
     *
     * @param AuthenticationPanel
     * @returns this
     */
    constructor: function(authenticationPanel) {
        this.app = authenticationPanel.app;
        this.authProviderIdPrefix = authenticationPanel.authProviderIdPrefix;
        this.accountsStorageIdPrefix = authenticationPanel.accountsStorageIdPrefix;
        this.autheticationPanel = authenticationPanel;
    },

    /**
     * OBS: This method must be overriten to get a new combo item for a custom backend
     *
     * Returns a combo item for authentication or accounts panel
     *
     * Example:
     * return {
     *      id: this.[authProviderIdPrefix|accountsStorageIdPrefix] + 'CustomBackendName',
     *      layout: 'form',
     *      autoHeight: 'auto',
     *      defaults: {
     *          width: 300,
     *          xtype: 'textfield',
     *          tabIndex: this.getTabIndex
     *      },
     *      items: []
     *  };
     *
     * @param {type} commonComboConfig
     * @returns Object
     */
    getCustomItems: function(commonComboConfig) {
        return null;
    },
});
