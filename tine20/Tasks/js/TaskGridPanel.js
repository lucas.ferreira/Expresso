/*
 * Tine 2.0
 * 
 * @package     Tasks
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.namespace('Tine.Tasks');

/**
 * Tasks grid panel
 * 
 * @namespace   Tine.Tasks
 * @class       Tine.Tasks.TaskGridPanel
 * @extends     Tine.widgets.grid.GridPanel
 * 
 * <p>Tasks Grid Panel</p>
 * <p><pre>
 * </pre></p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Tasks.TaskGridPanel
 */
Tine.Tasks.TaskGridPanel = Ext.extend(Tine.widgets.grid.GridPanel, {
    /**
     * record class
     * @cfg {Tine.Tasks.Model.Task} recordClass
     */
    recordClass: Tine.Tasks.Model.Task,
    
    /**
     * @private grid cfg
     */
    defaultSortInfo: {field: 'due', dir: 'ASC'},
    gridConfig: {
        clicksToEdit: 'auto',
        quickaddMandatory: 'summary',
        validate: true,
        autoExpandColumn: 'summary',
        // drag n drop
        enableDragDrop: true,
        ddGroup: 'containerDDGroup'
    },
    
    // specialised translations
    // ngettext('Do you really want to delete the selected task?', 'Do you really want to delete the selected tasks?', n);
    i18nDeleteQuestion: ['Do you really want to delete the selected task?', 'Do you really want to delete the selected tasks?'],
    
    /**
     * @private
     */
    initComponent: function() {
        this.recordProxy = Tine.Tasks.JsonBackend;
        this.gridConfig.cm = this.getColumnModel();
        
        this.defaultFilters = [
            {field: 'container_id', operator: 'equals', value: {path: Tine.Tinebase.container.getMyNodePath()}}
        ];
        Tine.Tasks.TaskGridPanel.superclass.initComponent.call(this);
        
        // the editGrids onEditComplete calls the focusCell after a edit operation
        // this leads to a 'flicker' effect we dont want!
        // mhh! but disabling this, breaks keynav 
        //this.grid.view.focusCell = Ext.emptyFn;
    },
    
    /**
     * returns cm
     * @return Ext.grid.ColumnModel
     * @private
     */
    getColumnModel: function(){

        var columns = [{id: 'tags', header: this.app.i18n._('Tags'), width: 40,  dataIndex: 'tags', sortable: false, renderer: Tine.Tinebase.common.tagsRenderer}];

        if (Tine.hasOwnProperty('Crm') && Tine.Tinebase.common.hasRight('view', 'Crm')) {
            columns.push({
                id: 'lead',
                header: this.app.i18n._('Lead name'),
                width: 150,
                dataIndex: 'relations',
                renderer: Tine.widgets.grid.RendererManager.get('Tasks', 'Task', 'lead'),
                sortable: false
            });
        }

        columns = columns.concat([{
            id: 'summary',
            header: this.app.i18n._("Summary"),
            width: 400,
            dataIndex: 'summary',
            quickaddField: new Ext.form.TextField({
                emptyText: this.app.i18n._('Add a task...')
            })
        }, {
            id: 'start_time',
            header: this.app.i18n._("Start Date"),
            width: 145,
            dataIndex: 'start_time',
            renderer: Tine.Tinebase.common.dateTimeRenderer,
            editor: new Ext.ux.form.DateTimeField({
                defaultTime: '12:00',
                allowBlank: true,
                listeners: {scope: this, change: this.onAfterEditStart}
            }),
            quickaddField: new Ext.ux.form.DateTimeField({
                defaultTime: '12:00',
                allowBlank: true,
                listeners: {scope: this, change: this.onAfterEditQuickAdd}
            })
        }, {
            id: 'due',
            header: this.app.i18n._("Due Date"),
            width: 145,
            dataIndex: 'due',
            renderer: Tine.Tinebase.common.dateTimeRenderer,
            editor: new Ext.ux.form.DateTimeField({
                defaultTime: '12:00',
                allowBlank: true,
                listeners: {scope: this, change: this.onAfterEdit}
            }),
            quickaddField: new Ext.ux.form.DateTimeField({
                defaultTime: '12:00',
                allowBlank: true,
                listeners: {scope: this, change: this.onAfterEditQuickAdd}
            })
        }, {
            id: 'priority',
            header: this.app.i18n._("Priority"),
            width: 65,
            dataIndex: 'priority',
            renderer: Tine.Tinebase.widgets.keyfield.Renderer.get('Tasks', 'taskPriority'),
            editor: {
                xtype: 'widget-keyfieldcombo',
                app: 'Tasks',
                keyFieldName: 'taskPriority'
            },
            quickaddField: new Tine.Tinebase.widgets.keyfield.ComboBox({
                app: 'Tasks',
                keyFieldName: 'taskPriority',
                value: 'NORMAL'
            })
        }, {
            id: 'percent',
            header: this.app.i18n._("Percent"),
            width: 50,
            dataIndex: 'percent',
            renderer: Ext.ux.PercentRenderer,
            editor: new Ext.ux.PercentCombo({
                autoExpand: true,
                blurOnSelect: true
            }),
            quickaddField: new Ext.ux.PercentCombo({
                autoExpand: true
            })
        }, {
            id: 'status',
            header: this.app.i18n._("Status"),
            width: 85,
            dataIndex: 'status',
            renderer: Tine.Tinebase.widgets.keyfield.Renderer.get('Tasks', 'taskStatus'),
            editor: {
                xtype: 'widget-keyfieldcombo',
                app: 'Tasks',
                keyFieldName: 'taskStatus'
            },
            quickaddField: new Tine.Tinebase.widgets.keyfield.ComboBox({
                app: 'Tasks',
                keyFieldName: 'taskStatus',
                value: 'NEEDS-ACTION'
            })
        }, {
            id: 'creation_time',
            header: this.app.i18n._("Creation Time"),
            hidden: true,
            width: 90,
            dataIndex: 'creation_time',
            renderer: Tine.Tinebase.common.dateTimeRenderer
        }, {
            id: 'completed',
            header: this.app.i18n._("Completed"),
            hidden: true,
            width: 90,
            dataIndex: 'completed',
            renderer: Tine.Tinebase.common.dateTimeRenderer
        }, {
            id: 'organizer',
            header: this.app.i18n._('Responsible'),
            width: 200,
            dataIndex: 'organizer',
            renderer: Tine.Tinebase.common.accountRenderer,
            quickaddField: Tine.widgets.form.RecordPickerManager.get('Addressbook', 'Contact', {
                userOnly: true,
                useAccountRecord: true,
                blurOnSelect: true,
                selectOnFocus: true,
                allowEmpty: true,
                value: Tine.Tinebase.registry.get('currentAccount')
            })
          // TODO add customfields and modlog columns, atm they break the layout :(
        }]);/*.concat(this.getModlogColumns().concat(this.getCustomfieldColumns()))*/

        return new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                resizable: true
            },
            columns: columns
        });
    },

    /**
     * return lead name for first linked Crm_Model_Lead
     *
     * @param {Object} data
     * @return {String} lead name
     */
    leadRenderer: function(data) {

        if( Ext.isArray(data) && data.length > 0) {
            var index = 0;
            // get correct relation type from data (contact) array and show first matching record (org_name + n_fileas)
            while (index < data.length && data[index].related_model != 'Crm_Model_Lead') {
                index++;
            }
            if (data[index]) {
                var name = (data[index].related_record.lead_name !== null ) ? data[index].related_record.lead_name : '';
                return Ext.util.Format.htmlEncode(name);
            }
        }
    },

    /**
     * is called on after edit quick add  to set related records
     * @param {} o
     */
    onAfterEditQuickAdd: function(o) {

            var due;
            var start;
            var dueField;
            var startField;

            Ext.each(this.getCols(true), function(item){
                if(Ext.isDate(item.lookup.due.quickaddField.value)){
                       dueField = item.lookup.due.quickaddField;
                       due = item.lookup.due.quickaddField.value;
                 }
                 if(Ext.isDate(item.lookup.start_time.quickaddField.value)){
                       startField = item.lookup.start_time.quickaddField;
                       start = item.lookup.start_time.quickaddField.value;

                 }

            }, this);

            if (Ext.isDate(start) && Ext.isDate(due) && due.getTime() - start.getTime() <= 0) {
                     o.markInvalid(this.app.i18n._('End date must be after start date'));
                     return false;
             } else {
                       if (Ext.isDate(due)) {
                        dueField.clearInvalid();
                       }
                       if (Ext.isDate(start)) {
                        startField.clearInvalid();
                       }
                        return true;
            }


    },

    /**
     * is called on after edit start time to set related records
     * @param {} o
     */

    onAfterEditStart: function(o) {

            var due;
            var start;
            var dueField;
            var startField;

            Ext.each(this.getCols(true), function(item){
                if(Ext.isDate(item.lookup.start_time.editor.record.data.due)){
                       dueField = item.lookup.start_time.editor.record.data.due;
                       due = item.lookup.start_time.editor.record.data.due;
                 }
                 if(Ext.isDate(o.value)){
                       startField = o.value;
                       start = o.value;

                 }

            }, this);

            if (Ext.isDate(start) && Ext.isDate(due) && due.getTime() - start.getTime() <= 0) {
                     o.markInvalid(this.app.i18n._('End date must be after start date'));
                     return false;
             } else {
                        return true;
            }


    },

    /**
     * is called on after edit due time to set related records
     * @param {} o
     */
    onAfterEdit: function(o) {

            var due;
            var start;
            var dueField;
            var startField;

            Ext.each(this.getCols(true), function(item){
                if(Ext.isDate(o.value)){
                       dueField = o.value;
                       due = o.value;
                 }
                 if(Ext.isDate(item.lookup.due.editor.record.data.start_time)){
                       startField = item.lookup.due.editor.record.data.start_time;
                       start = item.lookup.due.editor.record.data.start_time;

                 }

            }, this);

            if (Ext.isDate(start) && Ext.isDate(due) && due.getTime() - start.getTime() <= 0) {
                     o.markInvalid(this.app.i18n._('End date must be after start date'));
                     return false;
             } else {
                        return true;
            }


    },

    /**
     * is called by onStoreNewEntry to save record
     * and send notification to organizer
     *
     * @param {Object} recordData
     * @param {boolean} sendNotification
     * @return {Boolean}
     */
    saveEntry: function(record, sendNotification) {
        record.data.send = sendNotification;
        this.store.insert(0 , [record]);

        if (this.usePagingToolbar) {
            this.pagingToolbar.refresh.disable();
        }
        this.recordProxy.saveRecord(record, {
            scope: this,
            success: function(newRecord) {
                this.store.suspendEvents();
                this.store.remove(record);
                this.store.insert(0 , [newRecord]);
                this.store.resumeEvents();

                this.addToEditBuffer(newRecord);

                this.loadGridData({
                    removeStrategy: 'keepBuffered'
                });
            }
        });

        return true;
    },

    /**
     * new entry event -> add new record to store
     * ask to send notification to organizer
     *
     * @param {Object} recordData
     * @return {Boolean}
     */
    onStoreNewEntry: function(recordData) {
        var initialData = null;
        if (Ext.isFunction(this.recordClass.getDefaultData)) {
            initialData = Ext.apply(this.recordClass.getDefaultData(), recordData);
        } else {
            initialData = recordData;
        }
        var record = new this.recordClass(initialData);

       var app = Tine.Tinebase.appMgr.get('Tasks'),
       prefs = app.getRegistry().get('preferences'),
       ask = prefs.get('sendNotificationsForOrganizer'),
       user = record.data.organizer,
       logged_user = Tine.Tinebase.registry.get('currentAccount');
       record.data.send = false;

       if ( ask == '10' && user !== logged_user.accountId ) {
            Ext.MessageBox.confirm(_('Confirm'), app.i18n._('Do you want to send a notification to organizer?'), function(btn,text) {
               confirmation = true;
               if (btn == 'yes') {
                    this.saveEntry(record, true);
                }
                else {
                    this.saveEntry(record, false);
                }
            }, this);
        } else if(ask == '20' && user !== logged_user.accountId){
            this.saveEntry(record, true);
        } else {
            this.saveEntry(record, false);
        }

        return true;
    },

     /**
     * gets columns
     *
     * @param {Boolean} visibleOnly
     * @return {Array}
     */
    getCols: function(visibleOnly) {
        if(visibleOnly === true){
            var visibleCols = [];
            Ext.each(this.gridConfig.cm, function(column) {
                if (! column.hidden) {
                    visibleCols.push(column);
                }
            }, this);
            return visibleCols;
        }
        return this.gridConfig.cm;
    },


    /**
     * Return CSS class to apply to rows depending upon due status
     * 
     * @param {Tine.Tasks.Model.Task} record
     * @param {Integer} index
     * @return {String}
     */
    getViewRowClass: function(record, index) {
        var due = record.get('due');
         
        var className = '';
        
        if(record.get('status') == 'COMPLETED') {
            className += 'tasks-grid-completed';
        } else  if (due) {
            var dueDay = due.format('Y-m-d');
            var today = new Date().format('Y-m-d');

            if (dueDay == today) {
                className += 'tasks-grid-duetoday';
            } else if (dueDay < today) {
                className += 'tasks-grid-overdue';
            }
            
        }
        return className;
    }
});
