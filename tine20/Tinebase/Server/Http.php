<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 *
 */

/**
 * HTTP Server class with handle() function
 *
 * @package     Tinebase
 * @subpackage  Server
 */
class Tinebase_Server_Http extends Tinebase_Server_Abstract implements Tinebase_Server_Interface
{
    /**
     * the request method
     *
     * @var string
     */
    protected $_method = NULL;

    /**
     * the constructor
     */
    public function __construct()
    {
        $this->_supportsSessions = true;
        parent::__construct();
    }

    /**
     * handler for HTTP api requests
     * @todo session expire handling
     * @see Tinebase_Server_Interface::handle()
     *
     * @return HTTP
     */
    public function handle(\Zend\Http\Request $request = null, $body = null)
    {
        $this->_request = $request instanceof \Zend\Http\Request ? $request : Tinebase_Core::get(Tinebase_Core::REQUEST);
        $this->_body    = $body !== null ? $body : fopen('php://input', 'r');

        $server = new Tinebase_Http_Server();
        $server->setClass('Tinebase_Frontend_Http', 'Tinebase');
        
        try {

            if (Tinebase_Session::sessionExists()) {
                try {
                    Tinebase_Core::startCoreSession();
                } catch (Zend_Session_Exception $zse) {
                    // expire session cookie for client
                    Tinebase_Session::expireSessionCookie();
                }
            }

            Tinebase_Core::initFramework();
            
            Tinebase_Core::getLogger()->INFO(__METHOD__ . '::' . __LINE__ .' Is HTTP request. method: ' . $this->getRequestMethod());
            
            // register addidional HTTP apis only available for authorised users
            if (Tinebase_Session::isStarted() && Tinebase_Auth::hasIdentity()) {

                Tinebase_Config_Manager::getInstance()->detectDomain(TRUE);

                if (empty($_REQUEST['method'])) {
                    $_REQUEST['method'] = 'Tinebase.mainScreen';
                }
                
                $applicationParts = explode('.', $this->getRequestMethod());
                $applicationName = ucfirst($applicationParts[0]);
                
                if(Tinebase_Core::getUser() && Tinebase_Core::getUser()->hasRight($applicationName, Tinebase_Acl_Rights_Abstract::RUN)) {
                    try {
                        $server->setClass($applicationName.'_Frontend_Http', $applicationName);
                    } catch (Exception $e) {
                        Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ ." Failed to add HTTP API for application '$applicationName' Exception: \n". $e);
                    }
                }
                
            } else {
                if (empty($_REQUEST['method'])) {
                    $_REQUEST['method'] = 'Tinebase.login';
                }
                
                // sessionId got send by client, but we don't use sessions for non authenticated users
                if (Tinebase_Session::sessionExists()) {
                    // expire session cookie on client
                    Tinebase_Session::expireSessionCookie();
                }
            }
            
            $this->_method = $this->getRequestMethod();
            
            $server->handle($_REQUEST);
        } catch (Tinebase_Exception_NotInstalled $nie) {
            $this->_dieAndDestroySession($nie, $nie->getMessage());
        } catch (Zend_Json_Server_Exception $zjse) {
            // invalid method requested or not authenticated
            Tinebase_Core::getLogger()->INFO(__METHOD__ . '::' . __LINE__ .' Attempt to request a privileged Http-API method without valid session from "' . Tinebase_AccessLog::getRemoteIpAddress());
            
            header('HTTP/1.0 403 Forbidden');

            exit;
            
        } catch (Exception $exception) {
            if (! is_object(Tinebase_Core::getLogger())) {
                // no logger -> exception happened very early
                $this->_dieAndDestroySession($e, 'Service Unavailable');
            }
            
            Tinebase_Exception::log($exception);
            
            try {
                // check if setup is required
                $setupController = Setup_Controller::getInstance();
                if ($setupController->setupRequired()) {
                    $this->_dieAndDestroySession($e, 'Application needs to be installed or updated');
                } else {
                    $this->_method = 'Tinebase.exception';
                }
                
                $server->handle(array('method' => $this->_method));
                
            } catch (Exception $e) {
                $this->_dieAndDestroySession($e, 'Service Unavailable');
            }
        }
    }
    
    /**
    * returns request method
    *
    * @return string|NULL
    */
    public function getRequestMethod()
    {
        if (isset($_REQUEST['method'])) {
            $this->_method = $_REQUEST['method'];
        }
        
        return $this->_method;
    }

    /**
     * @param Exception $exception
     * @param string $message
     */
    protected function _dieAndDestroySession($exception, $message)
    {
        error_log($exception);
        Tinebase_Session::destroyAndRemoveCookie();
        header('HTTP/1.0 503 Service Unavailable');
        die($message);
    }
}
