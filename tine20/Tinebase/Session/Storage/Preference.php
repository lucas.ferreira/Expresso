<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Session
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 SERPRO (http://www.serpro.gov.br)
 *
 */

/**
 * A proxy for storing preference values into session
 *
 * @package     Tinebase
 * @subpackage  Session
 */
class Tinebase_Session_Storage_Preference extends Tinebase_Session_Storage_Abstract
{
    /**
     *
     * @return Tinebase_Session_Storage_Preference
     */
    public static function getInstance()
    {
        if (NULL == self::$_instance){
            self::$_instance = new self();
            if (isset(Tinebase_Config::getInstance()->session->storePreferenceIntoSession)){
                $disabled = !Tinebase_Config::getInstance()->session->storePreferenceIntoSession;
                self::$_instance->setDisabled($disabled);
            }
        }
        return self::$_instance;
    }

    /**
     * remove a preference from current user session
     *
     * @param string $preference
     */
    public function removePreference($preference) {
        $accountId = (is_object(Tinebase_Core::getUser())) ? Tinebase_Core::getUser()->getId() : '0';
        $cacheId = Tinebase_Preference_Abstract::createPreferenceCacheId($preference, $accountId);
        $this->remove($cacheId);
    }
}