<?php
/**
 * Connectionconfig operations
 *
 * @package Tinebase
 * @subpackage Shard
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * interface for Connectionconfig
 * @package Tinebase
 * @subpackage Shard
 */
interface Tinebase_Shard_Connectionconfig_Interface
{
   /**
    * the singleton pattern
    *
    * @param array $_options
    * @param string $_database
    * @return Tinebase_Shard_Connectionconfig_Interface
    */
    public static function getInstance(array $_options, $_database);
    /**
     * get Connectionconfig
     *
     * @param string $_key
     * @return array | FALSE
     */
    public function getConnectionConfig($_key);
    /**
     * set Connectionconfig
     *
     * @param string $_key
     * @param array $_parameters
     * @return boolean
     */
    public function setConnectionConfig($_key, $_parameters);
    /**
    * remove Connectionconfig
    *
    * @param string $_key
    * @return boolean
    */
    public function removeConnectionConfig($_key);
}