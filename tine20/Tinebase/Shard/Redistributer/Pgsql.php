<?php
/**
 * Redistributer backend class for PostgreSQL
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * Redistributer backend class for PostgreSQL
 * @package     Shard
 */
class Tinebase_Shard_Redistributer_Pgsql extends Tinebase_Shard_Redistributer_Abstract
{
    /**
     * Constructor
     *
     * @param  string    $_database
     */
       function __construct($_database) {
           parent::__construct($_database);
       }
}
