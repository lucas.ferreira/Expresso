<?php
/**
 * Tine 2.0
 *
 * @package     Shard
 * @subpackage  Context
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Implements the Context to select strategy to get Shardkeys
 *
 * @package     Shard
 */
class Tinebase_Shard_Shardkey_Strategy_Context
{
    /**
     * Class name that implements selected strategy
     *
     * @param  string
     */
    private  $_strategy = NULL;

    /**
     * Enforce interface restrictions
     *
     * @param  Tinebase_Shard_Shardkey_Strategy_Interface $_strategy
     */
    private function instantiateStrategy(Tinebase_Shard_Shardkey_Strategy_Interface $_strategy)
    {
        unset($this->_strategy);
        $this->_strategy = $_strategy;
    }

    /**
     * Select strategy
     *
     * @param  string $_strategy
     * @return boolean || NULL
     */
    public function setStrategy($_strategy)
    {
        $strategyClass = 'Tinebase_Shard_Shardkey_Strategy_' . $_strategy;

        if (class_exists($strategyClass)) {
            $this->instantiateStrategy(new $strategyClass());
            return TRUE;
        }
        return NULL;
    }

    /**
     * Get shardKey from selected strategy
     *
     * @param  string $_database
     * @return string || NULL
     */
    public function getShardKey($_database)
    {
        return $this->_strategy->getShardKey($_database);
    }

    /**
     * Get All shardKeys associated with a virtualshard from selected strategy
     *
     * @param  string $_database
     * @param  string $_virtualShard
     * @return array || NULL || FALSE
     */
    public function getShardKeysByVirtualShard($_database, $_virtualShard)
    {
        return $this->_strategy->getShardKeysByVirtualShard($_database, $_virtualShard);
    }

    /**
     * Get All shardKeys from selected strategy
     *
     * @param  string $_database
     * @return array || NULL
     */
    public function getAllShardKeys($_database)
    {
        return $this->_strategy->getAllShardKeys($_database);
    }

    /**
     * Get All Filters for Shard Key
     *
     * @param  string $_shardKey
     * @return array
     */
    public function getFilters($_shardKey)
    {
        return $this->_strategy->getFilters($_shardKey);
    }
}
