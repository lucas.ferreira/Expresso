<?php
/**
 * Tine 2.0
 * 
 * @package     Webconference
 * @subpackage  Model
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * filters for contacts that are room organizers
 * 
 * @package     Webconference
 * @subpackage  Model
 */
class Webconference_Model_ContactOrganizerFilter extends Webconference_Model_ContactAttendeeFilter 
{
    /**
     * @var string class name of this filter group
     *      this is needed to overcome the static late binding
     *      limitation in php < 5.3
     */
    protected $_className = 'Webconference_Model_ContactOrganizerFilter';
    
    /**
     * filter fields for organizer
     * 
     * @var array
     */
    protected $_filterFields = array('organizer');
    
    /**
     * extract contact ids
     * 
     * @param Tinebase_Record_RecordSet $_events
     */
    protected function _getForeignIds($_rooms)
    {
        $contactIds = array();
        
        foreach ($_rooms as $room) {
            if ($this->_matchFilter($room, 'organizer', 'organizer')) {
                $contactIds[] = $room->organizer;
            }
        }
        
        $this->_foreignIds = array_unique($contactIds);
    }
}
