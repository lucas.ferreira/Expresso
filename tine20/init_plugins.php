<?php
/**
 * Tine 2.0
 *
 * Use this script to initialize plugins for frontend, controller and backend layers
 * or for groups and user class
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2008-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 */
Addressbook_Backend_Ldap::addOptionPlugin('Custom_Addressbook_Backend_Ldap_ExternalCatalog');

Tinebase_Session::setIpAddressValidator('Custom_Session_Validator_IpAddress');
Tinebase_AccessLog::setStrategy('Custom_AccessLog_Strategy_Default');
Tinebase_Model_AccessLog::setAdditionalValidators(array(
    'passwordhash'  => array('allowEmpty' => true)
));
Tinebase_Model_AccessLogFilter::setAdditionalFilterModels(array(
    'passwordhash'  => array('filter' => 'Tinebase_Model_Filter_Text')
));

Tinebase_PluginManager::attachFrontendPlugins(array(
        'verifyCertificate' => 'Custom_Plugins_DigitalCertificate',
        'getKeyEscrowCertificates' => 'Custom_Plugins_DigitalCertificate',
    ), 'Custom_Plugins_DigitalCertificate');

Tinebase_PluginManager::setPlugins(array(
    'User' => array('addPlugin' => 'Tinebase_User_Plugin_Samba'),
    'Group' => array('addPlugin' => 'Tinebase_Group_Plugin_Samba')
));

Tinebase_PluginManager::initPlugins(array('Tinebase_Http_UserAgent'));

Tinebase_PluginManager::setThirdPartyPlugins(array(
    'Syncroton_Model_Folder'    => array('addPlugin' => 'Custom_Syncroton_Model_Folder'),
    'Syncroton_Command_Sync'    => array('setPlugin' => 'Custom_Syncroton_Command_Sync'),
    'Zend_Db_Adapter_Pdo_Pgsql' => array('setCachePlugin' => 'Custom_Db_Adapter_Pdo_Pgsql_Cache')
));

// Multidomain configuration
Tinebase_Config_Manager::setMultidomain(TRUE);

// customization of database tables
Setup_Controller::addPlugin('Custom_Setup_Controller');
