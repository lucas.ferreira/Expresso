<?php
/**
 * Tine 2.0
 * 
 * @package     Custom
 * @subpackage  Model
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * class Custom_Model_DigitalCertificateValidation
 * 
 * @package     Custom
 * @subpackage  Model
 */
class Custom_Model_DigitalCertificateValidation extends Tinebase_Record_Abstract 
{
    /**
     * identifier
     * 
     * @var string
     */ 
    protected $_identifier = 'certificate';
    
    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Tinebase';
    
    /**
     * record validators
     *
     * @var array
     */
    protected $_validators = array(
        'certificate'       => array('allowEmpty' => false),
        'hash'              => array('allowEmpty' => true),
        'success'           => array('allowEmpty' => true),
        'messages'          => array('allowEmpty' => true),
    );

    /**
     *
     * @param String $_certificate
     * @param boolean $_dontSkip If true, don't skip validation
     * @param boolean $_addPem If true, adds certificate in PEM format to response
     * @return Custom_Model_DigitalCertificateValidation
     * @todo Change success attribute to isValid
     */
    public static function createFromCertificate($_certificate, $_dontSkip = FALSE, $_addPem = FALSE)
    {
        $objCertificate = Custom_Auth_ModSsl_Certificate_Factory::buildCertificate($_certificate, $_dontSkip);
        $objReturn = array(
            'certificate' => array(
                'serialNumber'  => $objCertificate->getSerialNumber(),
                'issuerCn'      => $objCertificate->getIssuerCn(),
                'cn'            => $objCertificate->getCn(),
                'email'         => $objCertificate->getEmail(),
                'validFrom'     => $objCertificate->getValidFrom(),
                'validTo'       => $objCertificate->getValidTo(),
            ),
            'hash'        => $objCertificate->getHash(),
            'success'     => $objCertificate->isValid(),
            'messages'    => $objCertificate->getStatusErrors(),
        );
        if ($_addPem === TRUE) {
            $objReturn['certificate']['pemCertificateData'] = $objCertificate->getPemCertificateData();
        }
        return new Custom_Model_DigitalCertificateValidation($objReturn);
    }
}
